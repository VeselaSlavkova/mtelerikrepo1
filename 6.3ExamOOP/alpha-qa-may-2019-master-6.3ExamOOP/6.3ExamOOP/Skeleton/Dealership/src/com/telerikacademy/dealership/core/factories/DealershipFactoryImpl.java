package com.telerikacademy.dealership.core.factories;

import com.telerikacademy.dealership.models.*;
import com.telerikacademy.dealership.models.contracts.*;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.models.common.enums.Role;

public class DealershipFactoryImpl implements DealershipFactory {

    public Car createCar(String make, String model, double price, int seats)
    {
        return new CarImpl(make, model, price, seats);
    }

    public Motorcycle createMotorcycle(String make, String model, double price, String category)
    {
        return new MotorcycleImpl(make, model, price, category);
    }

    public Truck createTruck(String make, String model, double price, int weightCapacity)
    {
        return new TruckImpl(make, model, price, weightCapacity);
    }

    public User createUser(String username, String firstName, String lastName, String password, String role)
    {
        return new UserImpl(username, firstName, lastName, password, Role.valueOf(role.toUpperCase()));
    }

    public Comment createComment(String content, String author)
    {
        return new CommentImpl(content, author);
    }
}
