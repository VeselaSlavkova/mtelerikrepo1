package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Truck;

import java.util.ArrayList;
import java.util.List;

public class TruckImpl extends VehicleBase implements Truck {
    private final static String WEIGHT_CAPACITY = "Weight Capacity";
    private final static int WEIGHT_CAPACITY_MIN=1;
    private final static int WEIGHT_CAPACITY_MAX=100;
    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super (make, model, price, VehicleType.TRUCK);
        setWeightCapacity (weightCapacity);
    }

    public static int getWeightCapacityMin() {
        return WEIGHT_CAPACITY_MIN;
    }

    public static int getWeightCapacityMax() {
        return WEIGHT_CAPACITY_MAX;
    }

    public void setWeightCapacity(int weightCapacity) {
        if (weightCapacity<WEIGHT_CAPACITY_MIN||weightCapacity>WEIGHT_CAPACITY_MAX){
            throw new IllegalArgumentException ();
        }
        this.weightCapacity = weightCapacity;
    }
    //or made with setter void ValidateWeightCapacity() /view UserImpl
    @Override
    protected String printAdditionalInfo() {
        return String.format ("%s: %dt",WEIGHT_CAPACITY,getWeightCapacity ());
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    public int getWheels() {
        return 8;
    }

    @Override
    public VehicleType getType() {
        return super.vehicleType;
    }

    @Override
    public void removeComment(Comment comment) {
        if (comment==null){
            throw new IllegalArgumentException ();
        }
        getComments ().remove (comment);
    }
    @Override
    public void addComment(Comment comment) {
        if (comment==null){
            throw new IllegalArgumentException ();
        }
        getComments ().add (comment);
    }
    @Override
    public List<Comment> getComments() {
        return new ArrayList<> ();
    }


    //look in DealershipFactoryImpl - use it to create proper constructor

}
