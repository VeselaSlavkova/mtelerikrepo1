package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;

import java.util.ArrayList;
import java.util.List;

public class CarImpl extends VehicleBase implements Car{
    private final static String SEAT_NUMBER = "Seats";
    private final static int SEAT_AMOUNT_MIN=1;
    private final static int SEAT_AMOUNT_MAX=10;
    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super (make, model, price, VehicleType.CAR);
        setSeats (seats);
    }

    public static int getSeatAmountMin() {
        return SEAT_AMOUNT_MIN;
    }

    public static int getSeatAmountMax() {
        return SEAT_AMOUNT_MAX;
    }

    public void setSeats(int seats) {
        if (seats<getSeatAmountMin ()||seats>getSeatAmountMax ()){
            throw new IllegalArgumentException ("Seats must be between 1 and 10!");
        }
        this.seats = seats;
    }
//or made with setter void ValidateSeats() /view UserImpl
    @Override
    protected String printAdditionalInfo()
    {
        return String.format ("%s: %d",SEAT_NUMBER,getSeats ());
    }

    @Override
    public int getSeats() {
        return seats;
    }

 @Override
    public int getWheels() {
        return vehicleType.getWheelsFromType ();
    }

    @Override
    public VehicleType getType() {
        return super.vehicleType;
    }

    @Override
    public void removeComment(Comment comment) {
if (comment==null){
    throw new IllegalArgumentException ();
}
super.comments.remove (comment);
    }

    @Override
    public void addComment(Comment comment) {
        if (comment==null){
            throw new IllegalArgumentException ();
        }
       super.comments.add (comment);
    }

    @Override
    public List<Comment> getComments() {

        return new ArrayList<> ();
    }


    //look in DealershipFactoryImpl - use it to create proper constructor


}
