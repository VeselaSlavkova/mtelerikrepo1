package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private final static int MAKE_LENGTH_MIN=2;
    private final static int MAKE_LENGTH_MAX=15;
    private final static double PRICE_MIN=0.0;
    private final static double PRICE_MAX=1000000.0;
    private String make;
    private String model;
    private double price;
    VehicleType vehicleType;
  private int wheels;
    //add fields
    public List<Comment> comments;

    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
setMake (make);
setModel (model);
setPrice (price);
setVehicleType (vehicleType);
this.wheels=vehicleType.getWheelsFromType ();
this.comments=new ArrayList<> ();
    }

    public static String getMakeField() {
        return MAKE_FIELD;
    }

    public static int getMakeLengthMin() {
        return MAKE_LENGTH_MIN;
    }

    public static int getMakeLengthMax() {
        return MAKE_LENGTH_MAX;
    }

    @Override
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        if (make==null||make.length ()<getMakeLengthMin ()||make.length ()>getMakeLengthMax ()){
            throw new IllegalArgumentException("Make must be between 2 and 15 characters long!");
        }
        this.make = make;
    }

    @Override
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (model==null) {
            throw new IllegalArgumentException ();
        }
        this.model = model;
    }

    @Override
    public double getPrice() {
        return price;
    }
    public static double getPriceMin() {
        return PRICE_MIN;
    }

    public static double getPriceMax() {
        return PRICE_MAX;
    }

    public void setPrice(double price) {
        if (price<getPriceMin ()||price>getPriceMax ()){
            throw new IllegalArgumentException ("Price must be between 0.0 and 1000000.0!");
        }
        this.price = price;
    }
    public VehicleType getVehicleType()
    {
        return vehicleType;
    }
    public void setVehicleType(VehicleType vehicleType) {
        if (vehicleType==null){
            throw new IllegalArgumentException ();
        }
        this.vehicleType = vehicleType;
    }

   @Override
    public void removeComment(Comment comment) {
    this.comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
   this.comments.add(comment);
        }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder ();

        builder.append (String.format ("%s:", this.getClass ().getSimpleName ().replace ("Impl", ""))).append (System.lineSeparator ());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append (String.format ("%s %s",MAKE_FIELD,getMake ())).append (System.lineSeparator ());
        builder.append (String.format ("%s %s",MODEL_FIELD,getModel ())).append (System.lineSeparator ());
        builder.append (String.format ("%s %d",WHEELS_FIELD,getWheels ())).append (System.lineSeparator ());

        builder.append (String.format ("%s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble (price))).append (System.lineSeparator ());

        if (!printAdditionalInfo ().isEmpty ()) {
            builder.append (printAdditionalInfo ()).append (System.lineSeparator ());
        }
        builder.append (printComments ());
        return builder.toString ();
    }

    //todo replace this comment with explanation why this method is protected:
    protected abstract String printAdditionalInfo();

    private String printComments() {
        StringBuilder builder = new StringBuilder ();

        if (comments.size () <= 0) {
            builder.append (String.format ("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append (String.format ("%s", COMMENTS_HEADER)).append (System.lineSeparator ());

            for (Comment comment : comments) {
                builder.append (comment.toString ());
            }

            builder.append (String.format ("%s", COMMENTS_HEADER));
        }

        return builder.toString ();
    }
}
