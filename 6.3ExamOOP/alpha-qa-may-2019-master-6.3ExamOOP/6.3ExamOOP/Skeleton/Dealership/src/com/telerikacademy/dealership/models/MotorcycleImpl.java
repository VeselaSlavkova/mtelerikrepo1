package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

import java.util.ArrayList;
import java.util.List;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {
    private final static String CATEGORY_TYPE = "Category";
    private final static int CATEGORY_LENGTH_MIN=3;
    private final static int CATEGORY_LENGTH_MAX=10;
    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super (make, model, price, VehicleType.MOTORCYCLE);
        setCategory (category);
    }

    public static int getCategoryLengthMin() {
        return CATEGORY_LENGTH_MIN;
    }

    public static int getCategoryLengthMax() {
        return CATEGORY_LENGTH_MAX;
    }

    public void setCategory(String category) {
        if (category.length ()<CATEGORY_LENGTH_MIN||category.length ()>CATEGORY_LENGTH_MAX){
            throw new IllegalArgumentException("Category must be between 3 and 10 characters long!");
        }
        this.category = category;
    }
    //or made with setter void ValidateCategory() /view UserImpl
    @Override
    protected String printAdditionalInfo() {
        return String.format ("%s: %s",CATEGORY_TYPE,getCategory ());
    }

    @Override
    public String getCategory() {
        return category;
    }


    @Override
    public int getWheels() {
        return 2;
    }

    @Override
    public VehicleType getType() {
        return super.vehicleType;
    }

    @Override
    public void removeComment(Comment comment) {
        if (comment==null){
            throw new IllegalArgumentException ();
        }
        getComments ().remove (comment);
    }

    @Override
    public void addComment(Comment comment) {
        if (comment==null){
            throw new IllegalArgumentException ();
        }
        getComments ().add (comment);
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<> ();
    }


    //look in DealershipFactoryImpl - use it to create proper constructor
}
