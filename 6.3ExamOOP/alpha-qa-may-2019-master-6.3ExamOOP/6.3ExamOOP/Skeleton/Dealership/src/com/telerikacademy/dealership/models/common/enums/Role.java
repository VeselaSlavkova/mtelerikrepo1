package com.telerikacademy.dealership.models.common.enums;

public enum Role {
    NORMAL(0),
    VIP (1),
    ADMIN (2);

    private final int roleCode;

    Role(int roleCode) {
        this.roleCode = roleCode;
    }

    @Override
    public String toString() {
        switch (this) {
            case ADMIN:
                return "Admin";
            case NORMAL:
                return "Normal";
            case VIP:
                return "VIP";
            default:
                return "";
        }
    }
}
