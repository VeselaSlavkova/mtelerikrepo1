package com.telerikacademy.agency.core.contracts;

import com.telerikacademy.agency.commands.contracts.Command;

import java.util.List;

public interface CommandParser {
    String parseCommand(String fullCommand);
    List<String> parseParameters(String fullCommand);
}
