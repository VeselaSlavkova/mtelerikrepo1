const showContenct = () => {
    const $contentDisplayValue = $('#comics_container').css('display');
    const $character_info = $('#comics_container');
  	$character_info.toggle();

};

(() => {
    $("#btn_batman").on("click", (event) => {
        console.log(event);
        showContenct();
    });
})();


(() => {
  $("#btnWeb").on("click", () => {
      $.get(
          `https://akabab.github.io/superhero-api/api/id/${$('#charID').val()}.json`,
          function (data) {
              const info = data.name[0];
              $("#txtSample").html(`
                <div id="api_container"</div>
                  <div class="api_wrapped">
                    <div id="images" class="api_left_container">
                       <img src="${data.images.sm}" class="avatar_img"/>
                    </div>
                    <div class="api_right_container">
                      <h2>${data.name}</h2>
                      <h2>${data.slug}</h2>
                    </div>
                    <div style="clear: both;"></div>
                  </div>
                  <div class="api_hidden">
                    <div class="api_wrapped">
                      <div id="appearance">
                        <p class="api_section_header">Appearance:</p>
                        <ul>
                          <li>Gender: ${data.appearance.gender}</li>
                          <li>Race: ${data.appearance.race}</li>
                          <li>Height: ${data.appearance.height[1]}</li>
                          <li>Weight: ${data.appearance.weight[1]}</li>
                          <li>Eye Color: ${data.appearance.eyeColor}</li>
                          <li>Hair Color: ${data.appearance.hairColor}</li>
                        </ul>
                      </div>  
                      <div id="powerstats"</div>
                        <p class="api_section_header">Powerstats:
                          <ul>
                            <li>Intelligence: ${data.powerstats.intelligence}</li>
                            <li>Strength: ${data.powerstats.strength}</li>
                            <li>Speed: ${data.powerstats.speed}</li>
                            <li>Durability: ${data.powerstats.durability}</li>
                            <li>Power: ${data.powerstats.power}</li>
                            <li>Combat: ${data.powerstats.combat}</li>
                          </ul>
                      </div>
                      <div style="clear: both;"></div>
                    </div>
                    <div id="biography">
                       <p class="api_section_header">Biography:</p>
                       <ul>
                         <li>Full Name: ${data.biography.fullName}</li>
                         <li>Alter Egos: ${data.biography.alterEgos}</li>
                         <li>Aliases: ${data.biography.aliases}</li>
                         <li>Place Of Birth: ${data.biography.placeOfBirth}</li>
                         <li>First Appearance: ${data.biography.firstAppearance}</li>
                         <li>Publisher: ${data.biography.publisher}</li>
                         <li>Alignment: ${data.biography.alignment}</li>
                       </ul>
                    </div>
                    <div id="work">
                       <p class="api_section_header">Work:</p>
                       <ul>
                        <li>Occupation: ${data.work.occupation}</li>
                        <li>Base: ${data.work.base}</li>
                       </ul>
                    </div>
                    <div id="connections">
                       <p class="api_section_header">Connections:</p>
                       <ul>
                        <li>Group Affiliation: ${data.connections.groupAffiliation}</li>
                        <li>Relatives: ${data.connections.relatives}</li>
                       </ul>
                    </div>
                  </div>
                </div>
              `);
              //clear error message
              $(".error").html("");
          }
      ).fail(function (err) {
          $(".error").html(JSON.stringify(err));
      });
  });
})();


// (() => {
//     $('#btnHide').on('click', (event) => {
//     	${'#api_container'}.hide();
//     });
// })();

(() => {
    $("#btnHide").on("click", (event) => {
		$('#api_container').hide();
    });
})();
