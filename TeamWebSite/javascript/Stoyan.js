  function hideFunction() {

    let x = document.getElementById('field');
    if (x.style.display === 'none') {
      x.style.display = 'initial';
    } else {
      x.style.display = 'none';
    }
  }
 
  let vid = document.getElementById("vtVideo");

  function playVid() {
    vid.play();
  }   

  function pauseVid() { 
    vid.pause();
  }
 