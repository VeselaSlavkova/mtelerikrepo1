package com.behavetestframework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.FirefoxDriverManager;

import java.util.concurrent.TimeUnit;


public class CustomWebDriverManager {
    public enum CustomWebDriverManagerEnum {
        INSTANCE;
        private WebDriver driver = setupBrowser();

        private WebDriver setupBrowser(){
           // System.setProperty("webdriver.gecko.driver", "C:\\Users\\ivo\\Downloads\\geckodriver-v0.25.0-win64\\geckodriver.exe");
            FirefoxDriverManager.getInstance().setup();
            WebDriver firefoxDriver = new FirefoxDriver();
            firefoxDriver.manage().window().maximize();
            firefoxDriver.manage().deleteAllCookies();
            firefoxDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            return firefoxDriver;
        }

        public void quitDriver() {
            if (driver != null) {
                driver.quit();
            }
        }

        public WebDriver getDriver() {
            return driver;
        }


    }
}
