package com.behavetestframework;

import com.behavetestframework.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions<n> {
    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver(){
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key){
        Utils.LOG.info("Clicking on element " + key);
        waitForElementVisible(key, 10);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        element.click();
    }

    public void typeValueInField(String value, String field) throws InterruptedException {
        Utils.LOG.info("Type "+ value+"in the " +field);
        waitForElementVisible(field, 50);
        clearField(field);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Utils.getConfigPropertyByKey(value));
    }

    //Method is implemented in typeValueInField(String value, String field)
    public void clearField(String field){
        Utils.LOG.info("Clear " +field);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Keys.CONTROL+"a");
        element.sendKeys(Keys.BACK_SPACE);
    }


    //############# WAITS #########
    public void waitForElementVisible(String locator, int seconds){
        Utils.LOG.info("Wait for visibility of "+ locator+"iuntil " +seconds+"seconds");
        WebDriverWait wait = new WebDriverWait(driver,seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }
    /*public void waitForElementVisible(String locator, int seconds){
        WebElement element= driver.findElement(By.xpath(Utils.getConfigPropertyByKey(locator)));
        WebDriverWait wait= new WebDriverWait(driver,seconds);
        wait.until(ExpectedConditions.visibilityOf(element));
    }*/

    //############# ASSERTS #########

    public void assertElementPresent(String locator){
        Utils.LOG.info("Xpath exists: " + locator);
        waitForElementVisible(locator, 10);
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }
    public void assertElementNotPresent(String locator) {
        Utils.LOG.info("Xpath does NOT exist: " + locator);
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }
    public void assertTextEquals(String expectedText, String locator){
        WebDriverWait wait= new WebDriverWait(driver,10);
        WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
        Assert.assertEquals(expectedText, textOfWebElement);
    }
}
