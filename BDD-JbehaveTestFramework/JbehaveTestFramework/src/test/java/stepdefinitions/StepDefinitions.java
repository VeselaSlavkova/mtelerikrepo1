package stepdefinitions;

import com.behavetestframework.UserActions;
import com.behavetestframework.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")

    public void clickElement(String element) {
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")

    public void typeValueInField(String value, String field) throws InterruptedException {
        actions.typeValueInField(value, field);
    }

    @Given("Element $locator is present")
    @When("Element $locator is present")
    @Then("Element $locator is present")
    public void assertElementPresent(String locator){
        actions.assertElementPresent((locator));
    }

    @Given("Element $locator is not present")
    @When("Element $locator is not present")
    @Then("Element $locator is not present")
    public void assertElementNotPresent(String locator){
        actions.assertElementNotPresent((locator));
    }

    @Given("Element $expectedText is equal to $locator element")
    @When("Element $expectedText is equal to $locator element")
    @Then("Element $expectedText is equal to $locator element")
    public void assertTextEquals(String expectedText, String locator){
   actions.assertTextEquals(expectedText,locator);

    }
}
