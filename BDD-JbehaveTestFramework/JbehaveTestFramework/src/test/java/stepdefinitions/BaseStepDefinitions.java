package stepdefinitions;

import com.behavetestframework.UserActions;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;


public class BaseStepDefinitions {
    @BeforeStories
    public void beforeStories() throws InterruptedException {
        UserActions.loadBrowser();
        Thread.sleep(100);
    }

    @AfterStories
    public void afterStories() {
        UserActions.quitDriver();
    }
}

