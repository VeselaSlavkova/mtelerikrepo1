Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: User create a comment to existing post
Given Element createTopicButton is present
When Click searchFirstTopicFromMainList element
And Click replyOnCommentButton element
And Type text11chars in createTopicTextArea field
And Click replyCreateCommentButton element
Then Element cancelReplyOnCommentButton is not present
And Click homepageSchoolTelerikAcademy element