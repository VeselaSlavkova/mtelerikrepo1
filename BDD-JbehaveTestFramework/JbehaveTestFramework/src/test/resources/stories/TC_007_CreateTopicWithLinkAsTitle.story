Meta:
@topic

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: User create topic with link as title
Given Element homepageSchoolTelerikAcademy is present
When Click homepageSchoolTelerikAcademy element
And Click createTopicButton element
And Type topicTitleTextLink in createTopicTitleField field
And Type text10chars in createTopicTextArea field
And Click createNewTopicButton element
Then Element NewlyCreatedTopic is present
And Element topicTitleTextLink is present
