Meta:
@topictest

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: User open new topic popup and camcel it
Given Element createTopicButton is present
When Click createTopicButton element
And Type topicTitle3chars in createTopicTitleField field
And Click replyAreaCancelButton element
Then Element createTopicButton is present