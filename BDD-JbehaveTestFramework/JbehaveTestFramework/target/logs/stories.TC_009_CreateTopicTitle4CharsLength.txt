
(stories/TC_009_CreateTopicTitle4CharsLength.story)
Meta:
@topic 

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal
Scenario: User try to create topic with Title length less than minimal alert
Given Element createTopicButton is present
When Click createTopicButton element (FAILED)
(org.openqa.selenium.ElementClickInterceptedException: Element <span class="d-button-label"> is not clickable at point (1452,207) because another element <div class="modal-backdrop in"> obscures it
Build info: version: '3.14.0', revision: 'aacccce0', time: '2018-08-02T20:19:58.91Z'
System info: host: 'VESELA-PC', ip: '192.168.0.192', os.name: 'Windows 10', os.arch: 'x86', os.version: '10.0', java.version: '1.8.0_221'
Driver info: org.openqa.selenium.firefox.FirefoxDriver
Capabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 69.0.3, javascriptEnabled: true, moz:accessibilityChecks: false, moz:buildID: 20191009172106, moz:geckodriverVersion: 0.26.0, moz:headless: false, moz:processID: 11628, moz:profile: C:\Users\Vesela\AppData\Loc..., moz:shutdownTimeout: 60000, moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, platformVersion: 10.0, rotatable: false, setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}
Session ID: 84ca17c8-f211-4bb2-a6e2-1e5505d1b8d6)
And Type topicTitle_4chars in createTopicTitleField field (NOT PERFORMED)
And Type text_Base_Length in createTopicTextArea field (NOT PERFORMED)
And Click createNewTopicButton element (NOT PERFORMED)
Then Element topicTitleLengthOutOfBoundaryAlert is present (NOT PERFORMED)


