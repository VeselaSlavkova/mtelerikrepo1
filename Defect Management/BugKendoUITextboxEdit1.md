### **Description**
Options in drop down menu of Format button connot be used. They are not active.

### **Steps to reproduce**

1. Log in to Kendo IU
2. Create new post
3. Type some text in textbox
4. Mark the text to format it
5. Click on 'Format' button
6. Select option "Heading 1"
7. Select option "Quotation"
8. Select option "Paragraph"
9. Select option "Heading 4"

### **Expected result**

After selection, the marked text in textbox changes depending on selected option

### **Actual result**

Format of the marked text in textbox does not change

### **Severity: High**
/Bug doesn`t block work in the textbox, but is very visible and irritating/

### **Prerequisites**

User must have an account to login

### **Additional info**

* Test environment - Kendo UI
* Test browser - Chrome, Opera
* Test OS - Windows 10

# Classification

* Label: BugTextboxEdit1
* Responsible item: Format button and its drop down menu
* Functionality: Text editor