### **Description**
Inserted link in textbox section cannot be open by double click on it. 

### **Steps to reproduce**

1. Log in to Kendo IU
2. Create new post
3. Click on 'Insert lik' button
4. Insert a link in texbox section
5. Click twice on the inserted link to open it

### **Expected result**

It opens the site that is represented by this link by double click on it

### **Actual result**

Site that is represented by this link does not open

### **Severity: Medium**
/User can open the link in new tab/window/

### **Prerequisites**

User must have an account to login

### **Additional info**

* Test environment - Kendo UI
* Test browser - Chrome, Opera
* Test OS - Windows 10

# Classification

* Label: BugTextboxEdit1
* Responsible item: Inserted link`s options for opening
* Functionality: Text editor