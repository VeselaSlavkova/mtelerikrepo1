### **Description**

Placeholder for calculated cost displays price in non decimal format

### **Steps to reproduce**

1. Go to [http://adam.goucher.ca/parkcalc](http://adam.goucher.ca/parkcalc/index.php?Lot=STP&EntryTime=12%3A00&EntryTimeAMPM=AM&EntryDate=10%2F10%2F2019&ExitTime=14%3A00&ExitTimeAMPM=AM&ExitDate=10%2F12%2F2019&action=calculate&Submit=Calculate)
2. Look at Placeholder for calculated cost

### **Expected result**

Calculated cost displays price in decimal format

### **Actual result**

Calculated cost displays price in non decimal format

### **Severity: Low** 

### **Additional info**

* Test browser - Chrome, Opera
* Test OS - Windows 10

### Classification

* Label: BugCarParkingVideo
* Responsible item: Placeholder for calculated cost
* Functionality: Display calculated cost for parking