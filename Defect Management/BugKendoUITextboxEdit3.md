### **Description**
Inserted link in textbox section is not visible as a link. 

### **Steps to reproduce**

1. Log in to Kendo IU
2. Create new post
3. Type some text in textbox section
4. Click on 'Insert lik' button
5. Insert a link in texbox section
6. Find an inserted link in the text without hover the link

### **Expected result**

Inserted link is underlined and colored in blue for easyer use

### **Actual result**

Inserted link looks as other text. It cannot be find if it is not hover. 

### **Severity: Medium**
/User can find the link anyway/

### **Prerequisites**

User must have an account to login

### **Additional info**

* Test environment - Kendo UI
* Test browser - Chrome, Opera
* Test OS - Windows 10

# Classification

* Label: BugTextboxEdit1
* Responsible item: Inserted link`s display
* Functionality: Text editor