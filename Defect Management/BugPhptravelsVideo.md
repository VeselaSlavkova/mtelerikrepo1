### **Description**
Language Button do not load translated page in GERMAN. Page stays displayed in English.

### **Steps to reproduce**

1. Go to [www.phptravels.net](https://www.phptravels.net)
2. Go to the button for selection a language on the top right side of the page
3. Click on the button and select GERMAN

### **Expected result**

Page re-loads in German language

### **Actual result**

Correct page is loaded but the text is still in English and NOT translated to the required Language

### **Severity: Medium** 
/Bug doesn`t block using the site, but is very visible and irritating/

### **Additional info**

* Test environment -  [TEST VIDEO](https://www.youtube.com/watch?v=d3gwOP8PwKc&feature=youtu.be)
* Test browser - Chrome, Opera
* Test OS - Windows 10

### **Classification**

* Label: BugPhptravelsVideo
* Responsible item: Select Language drop down menu
* Functionality: Using site in optional languages