### **Description**
Placeholder for calculated cost displays incorrect TIME for parking

### **Steps to reproduce**

1. Go to [http://adam.goucher.ca/parkcalc](http://adam.goucher.ca/parkcalc/index.php?Lot=STP&EntryTime=12%3A00&EntryTimeAMPM=AM&EntryDate=10%2F10%2F2019&ExitTime=14%3A00&ExitTimeAMPM=AM&ExitDate=10%2F12%2F2019&action=calculate&Submit=Calculate)
2. In the input field for *Choose a Lot*, select *Short-term parking*
3. In the input field for *Entry Date and time*, set *date 8/10/2019 and time 12:00 AM*
4. In the input field for *Leaving Date and time*, set *date 8/11/2019 and time 01:00 AM*
5. On click *'Calculate'* button

### **Expected result**

Placeholder for calculated cost displays TIME for parking (0 Days, 13 Hours, 0 Minutes)

### **Actual result**

In placeholder for calculated cost is shown incorrect TIME for parking (1 Days, 1 Hours, 0 Minutes)

### **Severity: Critical** 
/Bug doesn`t block using the site - calculated price is correct for displayed time, but the calculated parking time is incorrect/

### **Additional info**

* Test browser - Chrome, Opera
* Test OS - Windows 10

### Classification

* Label: BugCarParkingVideo
* Responsible item: TIME parking calculator
* Functionality: Display  TIME for parking