### **Description**
Placeholder for Leaving time displays incorrect TIME format when user set time out of 12 Hour Clock (AM/PM) 

### **Steps to reproduce**

1. Go to [http://adam.goucher.ca/parkcalc](http://adam.goucher.ca/parkcalc/index.php?Lot=STP&EntryTime=12%3A00&EntryTimeAMPM=AM&EntryDate=10%2F10%2F2019&ExitTime=14%3A00&ExitTimeAMPM=AM&ExitDate=10%2F12%2F2019&action=calculate&Submit=Calculate)
2. In the input field for *Choose a Lot*, select *Short-term parking*
3. In the input field for *Entry Date and time*, set *date 8/10/2019 and time 12:00 AM*
4. In the input field for *Leaving Date and time*, set *date 8/10/2019 and time 14:00 PM*
5. On click *'Calculate'* button

### **Expected result**

Here is used 12 Hour Clock, so it must be displayed ERROR message or INSTRUCTION message "Set time between 0 and 12'

### **Actual result**

No message dets displayed. Leaving time is changed to invalid time format 26:00

### **Severity: Critical** 
/Bug doesn`t block using the site, but user can not be sure about the period of time behind the price/

### **Additional info**

* Test environment -  Video with steps to reproduce the issue: [HERE](https://www.youtube.com/watch?v=FgGPHWzd1dw)
* Test browser - Chrome, Opera
* Test OS - Windows 10

### Classification

* Label: BugCarParkingVideo
* Responsible item: input field for Leaving Date and time
* Functionality: Display Leaving time