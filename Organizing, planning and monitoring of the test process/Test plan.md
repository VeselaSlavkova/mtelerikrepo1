### **TEST PLAN**

##### **Prepared by: Vesela Slavkova**
##### **Date: 8/27/2019**

**1. Test plan identifier**

“Phase Test Plan for testing a forum topic`s creation functionality TP_1.0”

**2. Introduction**

This document outlines the high-level test strategy for phase of testing a forum topic`s creation functionality. 
The Test Plan has been created to define the objectives, scope, schedule, risks and approach. This document will clearly identify what the test deliverables will be and what is deemed in and out of scope.

**3. Objectives and tasks**

* Objectives 

The main goal of this Test plan is to verify that the forum topic`s creation functionality is developed according to the requirements.

* Tasks
    * Create the necessary test cases
    * Execute all the test cases
    * Report the result up to two days after execution as follow:
        * Number of executed tests ratio PASS/FAIL by priority
        * Bugs by feature
        * Bugs by severity
        * Logged vs. fixed issues
        
**4. Scope**

Test item that will be tested is forum topic`s creation functionality

**FEATURES TO BE TESTED**

*Priority 1* 
1. Options for creation of a topic in a forum 

*Priority 2* 
1. Options for edition and canceling of the topic in a forum

**FEATURES NOT TO BE TESTED**
1. Login in the forum
2. Options for creation of a subtopics under the new topic

**5. Types of testing**
* End to end testing–This functional testing will be used to assure that all required functionalities  are developed as expected
* Regression testing-This functional and non-functional testing will be used to assure that previously developed and already tested software still performance after addition  functionality of creation a new topic in a forum  
* Usability testing–This testing will be used to assure that creation of a new topic`s in a forum functionality is easy to use

**6. Environments and Processes**

The necessary and desired properties of the test environment are as follow:
* Hardware requirements
    * Personal Computers
    * Mobile devices
* Software requirements
    * Windows OS
    * Mac OS
    * Android OS
    * Software licenses
* Applications /Web browsers/
    * Chrome
    * Firefox
    * IE
    * Opera
        
**7. Entry Criteria**
* Functionality of creating of a new topic in a forum is deployed in environment
* The required access is provided
* The test data is prepared
* Required devices are prepared

**8. Exit Criteria**
* Test cases with *Priority 1* are executed and pass *100%*
* Test cases with *Priority 2* are executed and pass *80%*
* Time is run out
* When all blocked, critical and high bugs are fixed and verified

**9. QA Effort, Tracking and Reporting**

QA Responsibilities in the development lifecycle
* Manual verification of functionality of creating a new topic in a forum
* Monitor test execution
* Report inconsistencies, discrepancies between expected and actual behavior of tested functionality
* Prepare reports of test coverage, bug fixed, logged, etc.
* Help out with bug triaging

**10. Schedules and Responsibilities**

Deliverable | Responsibility | Completion date
----------- | -------------- | ---------------
Create  a Test plan | Test Manager, QAs | 	Two days after start the test planning
Develop Test cases, Prepared test specifications and test data | QAs, devs from FE team | Five days after acceptance the test plan
Set up needed devices, Cl jobs and repositories for test process | Test administrator /QA/ | Before start the test process
Execute Test cases | QAs | Until meet the exit criteria or time runs out
Test report | QAs, everyone testing the product | On going
Document and communicate test status | Test Manager, QAs | Weekly

**11. Tools**

Selenium, JIRA, Xray

**12. Risks**
* Delay in delivery in test plan, test cases, etc.
* Not enough resources
    
**13. APPROVALS**

Signature……………………

Name…………………………
    
Date…………………………
