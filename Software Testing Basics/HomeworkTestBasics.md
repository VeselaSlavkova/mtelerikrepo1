#### 1. Find as many bugs as you can on the ![Bug Example image](IssuesImage.jpg ) in the resources.

1. Border of the button "Sign up" is not the same color as the text inside
2. Text "Sign in" and button "Sign up" are not aligned on top
3. Button "Sign up", button " Login via Twitter" and button " Login via facebook" are not aligned on right
4. Logo of "Twitter" is not displayed
5. Text in button " Login via Twitter" is not correct. It has to be replaced with " Sign in with Twitter"
6. "facebook" starts with a small letter
7. Text in button " Login via facebook" is not correct. It has to be replaced with " Sign in with Facebook"
8. Placeholder "Email or login" is better be replaced with "username or Email"
9. Text in button "Log-in" is not correct. It has to be replaced with "Sign in".
10. Text in button "Log-in" is not centered.
11. Missed link about forgotten password.

#### 2. Create test cases covering the following functionality of a forum:

* Comments 

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a comment|**1**|**TC-1.0**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a comment with valid input|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| 

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.|Open a post  and click 'Reply" to comment to|  |A frame to comment a post gets displayed|
| 3.| In textbox type a comment|*Test comment*|‘Title field’is active for typing in it|
| 4.| Format text of the comment - some words make bold, other words make italic|*'**Test** COMMENT'*|Text is formated correctly|
| 5. |Attach a picture|(IssuesImage.jpg )|Attacment is successful|
| 6.| Click on ‘+ Reply’ button| |The comment is visible at bottom of the commented post|
-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a comment with empty textbox|**1**|**TC-1.1**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a comment with empty textbox|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| 

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.|Open a post  and click 'Reply" to comment to|  |A frame to comment a post gets displayed|
| 3.| In the textbox type nothing||It shows **Message** *Text must be at least ten characters*|
| 6.| Click on ‘+ Reply’ button| |No reply is posted|
-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Cancel a comment|**1**|**TC-1.2**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Cancel a comment|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| 

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.|Open a post  and click 'Reply" to comment to|  |A frame to comment a post gets displayed|
| 3.| Click on ‘Cancel’ button| |No reply is posted. A frame to comment a post closed |
-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Edit and Delete a comment|**2**|**TC-1.3**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Edit and Delete a comment|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| 

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.|Open a post  and click 'Reply" to comment to|  |A frame to comment a post gets displayed|
| 3.| In textbox type a comment|*Test comment*|‘Title field’is active for typing in it|
| 4.| Click on ‘+ Reply’ button| |The comment is visible at bottom of the commented post|
| 5.| Open the reply you have just posted| |The reply is active for edition|
| 4.| Edit text of the comment|*'**Test** COMMENT'*|Text is edited correctly|
| 5. |Click on *delete* button||Comment is terminated|
-------------

* Creation of a new topic

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a new topic|**1**|**TC-2.0**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with valid input|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.|Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
| 3.|Enter topic title in ‘Title field’|*Test topic*|‘Title field’is active for typing in it|
| 4.| Enter text in text field|*Find all as many bugs as you can*|‘Textbox field’is active for typing in it|
| 5.| Format text in text field - some words make bold, other words make italic|*Find* all **as many bugs** as you can|Text is formated correctly|
| 6.|Attach a picture|![Bug Example image](IssuesImage.jpg )|Attacment is successful|
| 7.|In the field for category make your choice from drop-down menu|*Site feedback*|Selected category is displayed  in the relevant field|
| 8.|Enter a tag a in the field for tags|*Homework*|The new Topic is already visible for all users of the relevant tag
| 9.| Click "+ Create Topic" button| | The new Topic is visible for all users|

-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
|-------------|----------------|-------------|----------------|
|Creation of a new topic with empty title and empty text section|**1**|**TC-2.1**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with empty field for title and empty text section|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.|Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
| 3.| In the field for title type nothing||Pop up **Message** *Title is required*|
| 4.|In the field for text type nothing||Pop up **Message** *Post can``t be empty*|
| 9.| Click "+ Create Topic" button| | The new Topic is not created|

-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
|-------------|----------------|-------------|----------------|
|Creation of a new topic with under sized title and under sized text section|**1**|**TC-2.2**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with under sized title /4 characters/ and under sized text section /9 characters/|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.|Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
| 3.|Enter text in text field|*Test*|Pop up **Message** *Title must be at least 5 characters*|
| 4.|Enter text in text field|*Test post*|Pop up **Message** *Post must be at least 10 characters*|
| 9.| Click "+ Create Topic" button| | The new Topic is not created|

-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Create a new topic with oversized title and oversized text section |**1**|**TC-2.3**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with oversized title /256 characters/ and oversized text section (32001 characters)
|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1. |Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.| Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3. |Enter topic title in ‘Title field’|*Take from https://www.lipsum.com/*|Pop up **Message** *Title must be no more than 255 characters*|
| 4. |Enter text in text field|*Take from https://www.lipsum.com/*|Pop up **Message** *Post must be no more than 32000 characters*|
| 5. |Click on ‘+ Create Topic’ button| |New Topic is not created|

-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Cancel creation of a new topic|**1**|**TC-2.4**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Cancel creation of a new topic,made by mistake|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1. |Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.| Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3. |Enter topic title in ‘Title field’|*Test123*|‘Title field’is active for typing in it|
| 4. |Enter text in text field|*Test post 123*|‘Textbox field’is active for typing in it|
| 5. |Click 'Cancel" button| |A frame to create a topic canceled.New Topic is not created|

-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a new topic from unregistered user|**2**|**TC-2.5**|26.08.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic from user with no valid account in the forum|Vesela Slavkova| ||
|**Pre requisite:**|The User has not valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Open home page of the forum|https://schoolforum.telerikacademy.com/ |Forum`s home page gets displayed|
| 2.|Click  "+ New Topic" button|  |There is no such a button on your disposal. New topic cannot be created|

-------------

#### 3. Prioritize the test cases for using a microwave.
#### Use priority levels from 1 to 3, where 1 means highest priority.

**PRIORITY LEVEL 1**
* The microwave starts when the start button is pressed.
* The microwave stops when the stop button is pressed.
* Power goes off while the microwave is working.

**PRIORITY LEVEL 2**
* Selection of program changes the mode of the microwave.
* The display shows accurate time.

**PRIORITY LEVEL 3**
* The light inside turns on when the door is opened