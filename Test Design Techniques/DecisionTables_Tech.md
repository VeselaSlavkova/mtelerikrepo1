|![TL](https://content.screencast.com/users/Stoio/folders/Default/media/3f6926f2-9df9-45ea-a7c5-7e520caa2d58/HORNETSdownload.jpg) | ![](https://content.screencast.com/users/Stoio/folders/Default/media/51135a1a-21b0-41f6-8e38-b3c2592c03b4/Tozispaceing.jpg)|
|:---:|:---:|

### **TASK 2**
**Decision tables for the notifications.**

To verify functionality of getting **notification** for a release as a user of *schoolforum.telerikacademy* is suitable to use
> Decision tables technique - test the logic when there are dissimilar inputs and  different combination of inputs which result in different actions.

| **Conditions** |**Step 1**|**Step 2**|**Step 3**|**Step 4**|**Step 5**|**Step 6**|**Step 7**|**Step 8**|
|------|------|------|------|------|------|------|------|------|
|The user is subscribed for notification |Y|Y|Y|Y|N|N|N|N|
|The user has enabled release for notifications|Y|Y|N|N|Y|Y|N|N|
|The user has the latest version|Y|N|Y|N|Y|N|Y|N|
|Get a notification|N|Y|N|N|N|N|N|N|

The final process of this method is to write test cases to use only these steps in the table, based of the logic of the functionality:

1. If condition 1 is not provided it will be designed only one test case to verify that the user did not get a notification
2. If condition 3 is provided it will be designed only one test case to verify that the user did not get a notification
3. If condition 1 and condition 2 are provided but condition 3 is not provided it will be designed test case to veriify that the user got a notification

-------------
**TestTitle**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Verify that user did not get a notification if it is no subscribtion for it|**2**|**TC-2.0**|02.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|User did not get a notification if he/she is not subscribed for notifications|Vesela Slavkova| ||
|**Pre requisite:**|It is used two independent valid user accounts UA1 ( for User 1) and UA2 (for User 2)|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
 |-------------|----------------|-------------|----------------|
| 1. |Log in with UA1 |https://schoolforum.telerikacademy.com/ |The User 1 logged in successfully|
| 2. |User 1 create a topic|*Test topic.*|The new topic is visible for all users of the forum|
| 3.| User 1 click on "Enable Notification" button at the top of the page and select "Allow" |  |User 1 will be notified of anything about this topic|
| 4. |User 1 click on "Notify" button at the bottom of the topic and select "Muted" from drop-down menu|  |User 1 will be never notified of anything about this topic|
| 5.| Log in with UA2 |https://schoolforum.telerikacademy.com/ |The User 2 logged in successfully|
| 6.| User 2 go to the topic created by User 1|  |The new topic is active for replays, likes,etc.|
| 7.| User 2 click on "Reply" button in the bottom of the *Test reply*|  |A frame to create a replay gets displayed|
| 8. |User 2 enter text in text field|*Test text.*|‘Textbox field’ is active for typing in it|
| 9.| User 2 click on ‘+ Reply’ button| |New reply *Test1* with description *Test text.* is posted|
| 10.| User 1 click on his/her 'Profile' button|  |A frame of User`s 1 profile gets displayed|
| 11. |Click on 'Notifications' button| |User 1 has no notifications|

-------------
**TestTitle**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|User did not get a notification if there are no new posts in the forum|**2**|**TC-2.1**|02.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Verify that user did not get a notification if he/she has the latest version of the forum|Vesela Slavkova| ||
|**Pre requisite:**|It is used two independent valid user accounts UA1 ( for User 1) and UA2 (for User 2)|No replays has been posted after User`s 1 last visit to the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
 |-------------|----------------|-------------|----------------|
| 1. |Log in with UA1 |https://schoolforum.telerikacademy.com/ |The User 1 logged in successfully|
| 2. |User 1 create a topic|*Test topic.*|The new topic is visible for all users of the forum|
| 3. |User 1 click on "Enable Notification" button at the top of the page and select "Allow" |  |User 1 will be notified of anything about this topic|
| 4.| User 1 click on "Notify" button at the bottom of the topic and select "Watching" from drop-down menu|  |User 1 will be notify for every new replay in this topic|
| 5. |User 1 click on his/her 'Profile' button|  |A frame of User`s 1 profile gets displayed|
| 6. |Click on 'Notifications' button| |User 1 has no notifications|

------------

**TestTitle**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|User got a notification if he/she is subscribed for notifications and has enabled release for notifications|**2**|**TC-2.2**|02.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Verify that user got a notification if he/she is subscribed for notifications and has enabled release for notifications| ||
|**Pre requisite:**|It is used two independent valid user accounts UA1 ( for User 1) and UA2 (for User 2)|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
 |-------------|----------------|-------------|----------------|
| 1. |Log in with UA1 |https://schoolforum.telerikacademy.com/ |The User 1 logged in successfully|
| 2. |User 1 create a topic|*Test topic.*|The new topic is visible for all users of the forum|
| 3. |User 1 click on "Enable Notification" button at the top of the page and select "Allow" |  |User 1 will be notified of anything about this topic|
| 4. |Click on "Notify" button at the bottom of the topic and select "Watching" from drop-down menu|  |User 1 will be notify for every new replay in this topic|
| 5. |Log in with UA2 |https://schoolforum.telerikacademy.com/ |The User 2 logged in successfully|
| 6. |User 2 go to the topic created by User 1|  |The new topic is active for replays, likes,etc.|
| 7. |User 2 click on "Reply" button in the bottom of the *Test reply*|  |A frame to create a replay gets displayed|
| 8. |User 2 enter text in text field|*Test text.*|‘Textbox field’ is active for typing in it|
| 9.| User 2 click on ‘+ Reply’ button| |New reply *Test1* with description *Test text.* is posted|
| 10. |User 1 click on his/her 'Profile' button|  |A frame of User`s 1 profile gets displayed|
| 11. |Click on 'Notifications' button| |User 1 has a notification|
-------------