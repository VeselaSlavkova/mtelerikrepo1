|![TL](https://content.screencast.com/users/Stoio/folders/Default/media/3f6926f2-9df9-45ea-a7c5-7e520caa2d58/HORNETSdownload.jpg) | ![](https://content.screencast.com/users/Stoio/folders/Default/media/51135a1a-21b0-41f6-8e38-b3c2592c03b4/Tozispaceing.jpg)|
|:---:|:---:|

### **TASK 1**
**Equivalence partitioning and Boundary Value analysis for the topic title
and description.**

To verify functionality of creation a new topic with valid **topic title** and **topic description** is suitable to use
> Equivalence partitioning technique - divide input data to valid and invalid parts

and
> Boundary value analysis technique - design test cases based on boundary values

1. Valid input length for topic title is **5-255** characters
2. Valid input length for topic text is **10-32 000** characters

| **Test** | **Invalid input** | **Valid input** | **Invalid input** |
|------------|---------|-----------------|--------------------|
|Valid input **length for topic title** in characters |0........4 | 5........255|256......|
|Boundary values for the **topic title`s length** in characters| ........4 | 5,6........254,255 | 256...... |
|Valid input **length for topic description** in characters|0........9 | 10........32000|32001......|
|Boundary values for the **description`s length** in characters|........9 | 10,11........31999,32000 | 32001...... |

-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Create a new topic with undersize input for title|**1**|**TC-1.0**|02.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Create a new topic with undersize input for title (4 characters)|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| 

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ | |The User logged in successfully|
| 2.| Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3. |Enter topic title in ‘Title field’|*Test*| It shows **Message** *Title must be at least five characters*|
| 4. |Enter text in text field|*Test text.*|‘Textbox field’is active for typing in it|
| 5. |Click on ‘+ Create Topic’ button| | New Topic is not be created  |
-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Create a new topic with valid input|**1**|**TC-1.1**|02.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Create new topic with valid input for title (5 characters) and valid input for description (10 characters)|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1. |Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.| Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3. |Enter topic title in ‘Title field’|*Test1*|‘Title field’is active for typing in it|
| 4.| Enter text in text field|*Test text.*|‘Textbox field’is active for typing in it|
| 5.| Select category from drop-down menu in 'category field' for the new topic| |New Topic *Test1* with description *Test text.* is displayed in the selected category|
| 6.\ Click on ‘+ Create Topic’ button| |New Topic *Test1* with description *Test text.* is created|
-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Create a new topic with oversized input for description |**1**|**TC-1.2**|02.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Create new topic with valid input for topic (255 characters) and invalid oversized input for description (32001 characters)
|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1. |Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.| Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3. |Enter topic title in ‘Title field’|*Take from https://www.lipsum.com/*|‘Title field’is active for typing in it|
| 4. |Enter text in text field|*Take from https://www.lipsum.com/*|It shows **Message** *Post must be no more than 32000 characters*|
| 5. |Click on ‘+ Create Topic’ button| |New Topic is not created|
-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Create a new topic with oversized input for topic |**1**|**TC-1.3**|02.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
| Create new topic with invalid oversized input for topic title (256 characters) and valid input for description (32000 characters)|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1. |Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2. |Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed||
| 3. |Enter topic title in ‘Title field’|*Take from* https://www.lipsum.com/|It shows **Message** *Title must be no more than 255 characters*|
| 4. |Enter text in text field|*Take from* https://www.lipsum.com/|‘Textbox field’is active for typing in it|
| 5. |Click on ‘+ Create Topic’ button| |New Topic is not created|
-------------