|![TL](https://content.screencast.com/users/Stoio/folders/Default/media/3f6926f2-9df9-45ea-a7c5-7e520caa2d58/HORNETSdownload.jpg) | ![](https://content.screencast.com/users/Stoio/folders/Default/media/51135a1a-21b0-41f6-8e38-b3c2592c03b4/Tozispaceing.jpg)|
|:---:|:---:|

### **TASK 3**
**Classification trees for the categories and labels.**

To verify functionality of **classification the topic of the forum based on category and label**, selected in its creation is suitable to use
>Classification trees technique - test cases, described by means of a Classification tree, are designed to execute combinations of representatives of input and/or output domains

![tree](Classification_tree.png)

 **Object of testing - Topic in a forum** is classified by submitting it to a series of tests that determine its class label. These tests are organized in a hierarchical structure following Divide-and-Conquer Algorithm.

 |**TestTitle**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a topic in selected category and one tag|**1**|**TC-3.0**|03.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with valid input in selected category and with selected one tag|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| ||

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
 |-------------|----------------|-------------|----------------|
|1. | Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2. |Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3.| Enter topic title in ‘Title field’|*Test1*|‘Title field’is active for typing in it|
| 4. |Enter text in text field|*Test text.*| Textbox is active for typing in it|
| 5. |Format the text in text field|**Test** TEST.*|Text is formated correctly|
| 6. |Select category from drop-down menu in 'category field' for the new topic|*Site feedback* |New Topic *Test1* with description *Test text.* is displayed in the selected category|
| 7. |Select tag from drop-down menu in 'tag field' for the new topic|*test* |New Topic is forward to the category *Site feedback*, with tag *test*|
| 8. |Click on ‘+ Create Topic’ button| |New Topic *Test1* with description *Test text.* is created in the category *Site feedback*, with tag *test*|
-------------


|**TestTitle**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a new topic in selected category and many tags|**1**|**TC-3.1**|03.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with valid input in selected category and with selected many tags|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| ||

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
 |-------------|----------------|-------------|----------------|
|1. | Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2. |Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3.| Enter topic title in ‘Title field’|*Test1*|‘Title field’is active for typing in it|
| 4. |Enter text in text field|*Test text.*| Textbox is active for typing in it|
| 5. |Format the text in text field|**Test** TEST.*|Text is formated correctly|
| 6. |Select category from drop-down menu in 'category field' for the new topic|*Site feedback* |New Topic *Test1* with description *Test text.*  is forward to the selected category|
| 7. |Select tag from drop-down menu in 'tag field' for the new topic|*test*, *homework*, *test-the-edit-option* |New Topic is forward to the category *Site feedback*, with tags *test*, *homework*, *test-the-edit-option*|
| 8. |Click on ‘+ Create Topic’ button| |New Topic *Test1* with description *Test text.* is created in the category *Site feedback*, with tags *test*, *homework*, *test-the-edit-option*|
-------------


|**TestTitle**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a new topic with selected two tags and no in specified category|**1**|**TC-3.2**|03.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with valid input with selected two tags and no in specified category|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| ||

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
 |-------------|----------------|-------------|----------------|
|1. | Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2. |Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3.| Enter topic title in ‘Title field’|*Test1*|‘Title field’is active for typing in it|
| 4. |Enter text in text field|*Test text.*| Textbox is active for typing in it|
| 5. |Format the text in text field|**Test** TEST.*|Text is formated correctly|
| 6. |Select nothing in 'category field'||New Topic *Test1* with description *Test text.* is forward to the category 'Uncategorized'|
| 7. |Select tag from drop-down menu in 'tag field' for the new topic|*test*, *homework* |New Topic is created with tags *test*, *homework*|
| 8. |Click on ‘+ Create Topic’ button| |New Topic *Test1* with description *Test text.* is visible in the category 'Uncategorized', with tags *test*, *homework*|
-------------

|**TestTitle**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Search a topic in the category and by tags,selected in creation of the topic|**1**|**TC-3.3**|03.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Search a topic in the specified category and by specified tags,selected in creation of the topic|Vesela Slavkova| ||
|**Pre requisite:**|The User has valid user account in the forum| ||

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
 |-------------|----------------|-------------|----------------|
|1. | Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2. |Click on ‘categories’ button|  |A dropdown menu for categories gets displayed|
| 3.| Choose the category, where is created the topic|*Site feedback* |All the topics in this category get displayed|
| 4. |Click on the created topic|| The topic page gets displayed |
| 5. |Go back to the home forum page||Home forum page gets displayed |
| 6. |Click on ‘tags’ button||A dropdown menu for tags gets displayed|
| 7. |Choose the first tag, relevant for the topic|*test* |All the topics tagged with *test* get displayed|
| 8. |Click on the created topic|| The topic page gets displayed |
| 5. |Go back to the home forum page||Home forum page gets displayed |
| 6. |Click on ‘tags’ button||A dropdown menu for tags gets displayed|
| 7. |Choose the second tag, relevant for the topic|*homework* |All the topics tagged with *homework* get displayed|
| 8. |Click on the created topic|| The topic page gets displayed |
-------------
