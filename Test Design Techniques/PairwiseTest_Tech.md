|![TL](https://content.screencast.com/users/Stoio/folders/Default/media/3f6926f2-9df9-45ea-a7c5-7e520caa2d58/HORNETSdownload.jpg) | ![](https://content.screencast.com/users/Stoio/folders/Default/media/51135a1a-21b0-41f6-8e38-b3c2592c03b4/Tozispaceing.jpg)|
|:---:|:---:|

### **TASK 4**
**Pairwise testing for the supported browsers, operational systems.**

To verify that users of the forum receives the same experience across various browsers and operational systems is suitable to use:

> Pairwise Test technique -test cases are designed to execute all possible discrete combinations of each pair of input parameters.

As it is nearly impossible to achieve 100% test coverage (test combinations for each browsers and each operational systems), test cases will be created after Classification the browsers /the latest versions/, based upon the amount of target users, in three zones

1. Zone 1 - The most used browsers
2. Zone 2 - Very used browsers
3. Zone 3 - Rarely used browsers

and test the combination with the most used operational systems:

| **OS**|**Browser**|
|------|-----------|
|| **Zone 1**|
| Windows | Chrome |
| Windows | Firefox |
| Mac OS | Safari  |
| Android| Chrome  |
| Android| Firefox |
| |**Zone 2** |
| Windows | Opera  |
| Windows | Edge  |
| |**Zone 3** |
| Windows | Internet Explorer |

The final process of this method is to write test cases to verify 100% of combinations for Zone 1, 80% of combinations for Zone 1 and, if it has time enough, combinations for Zone 3.

They are going to be executed **two test cases for each of the required combinations**.
**Definition the OS and the Browser for each test case is in *Pre requisite*.**

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a new topic with valid input|**1**|**TC-3.0**|03.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with valid input|Vesela Slavkova| ||
|**Pre requisite:**|**Windows OS** ir prepared for test execution|**Google Chrome** Home page is opened|The User has valid user account in the forum| 

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1.| Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.| Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3.| Enter topic title in ‘Title field’|*Test1*|‘Title field’is active for typing in it|
| 4.| Enter text in text field|*Test text.*| Textbox is active for typing in it|
| 5. |Format the text in text field|**Test** TEST.*|Text is formated correctly|
| 6. |Select category from drop-down menu in 'category field' for the new topic|*Site feedback* |New Topic *Test1* with description *Test text.* is displayed in the selected category|
| 7.| Click on ‘+ Create Topic’ button| |New Topic *Test1* with description *Test text.* is created|
-------------

**Test Title**|**Priority**|**Test Case ID:** |**Test design date**|
 |-------------|----------------|-------------|----------------|
|Creation of a new topic with invalid input and cancellation|**1**|**TC-3.1**|03.09.2019|
|**Test Case Description:**| **Test designed by:**|**Test executed by:**|**Test execution date**|
|Creation of a new topic with invalid input and cancellation the frame to create a topic|Vesela Slavkova| ||
|**Pre requisite:**|**Windows OS** ir prepared for test execution|**Google Chrome** Home page is opened|The User has valid user account in the forum|

|**No**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----------------|-------------|----------------|
| 1. |Log in |https://schoolforum.telerikacademy.com/ |The User logged in successfully|
| 2.| Click on ‘+ New Topic’ button|  |A frame to create a topic gets displayed|
| 3.| Enter topic title in ‘Title field’|*Test*|It shows **Message** *Title must be at least five characters*. New Topic is not be created |
| 4.| Enter text in text field|*Text 32001 characters - take from* https://www.lipsum.com/|It shows **Message** *Text must be no more than 32000 characters*|
| 5.| Click on ‘Cancel’ button| |A frame to create a topic closes. New Topic is not created|
-------------