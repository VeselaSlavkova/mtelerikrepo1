import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class testVideoPlay {

    @Test
    public void playVideo() throws InterruptedException {
        //set drivers
        System.setProperty("webdriver.gecko.driver", "D:\\GIT\\GIT_TestHomework\\alpha-qa-2019-homework\\vesela\\Automated testing SeleniumWebDriver\\searchVideoPlay\\searchVideoPlay\\src\\main\\resources\\geckodriver-v0.24.0-win32\\geckodriver.exe");
       // System.setProperty("webdriver.chrome.driver", "F:\\Modul3\\searchVideoPlay\\src\\main\\resources\\chromedriver_76\\chromedriver.exe");
        WebDriver driver = new FirefoxDriver();
       // WebDriver driver = new ChromeDriver();
        driver.get("https://www.youtube.com/watch?v=9fmkQg77m0o");
        //start the player
        WebElement videoPlay = driver.findElement(By.xpath("//*[@id='movie_player']"));
        videoPlay.click();
        //fullscreen
        WebElement fullScreen = driver.findElement(By.xpath("//button[@title='Full screen (f)']"));
        fullScreen.click();
        //listen to music
        Thread.sleep(10000);
        //exit fullscreen
        videoPlay.getLocation();
        fullScreen.sendKeys("f");
        //pause
        videoPlay.getLocation();
        videoPlay.sendKeys("k");
        //play
        videoPlay.getLocation();
        videoPlay.sendKeys("k");
        //like the video
        WebElement like = driver.findElement(By.xpath("//yt-icon[@class='style-scope ytd-toggle-button-renderer']"));
        like.click();
        driver.quit();
    }
}
