package testCases;

import org.junit.Test;

public class TC024_ConfirmEscXButtonWorks extends BaseTest {

    @Test
    public void confirmEscXButtonWorks() {
        actions.logIn();
        actions.signIn();
        actions.clickElement("xpath.createTopicButton");
        actions.inputNewTopicTitle("TestTestTest");
        actions.inputNewTopicMainText("TestTestTest");
        actions.clickElement("xpath.createTopicEscXButton");
        actions.assertElementNotPresent("xpath.createTopicEscXButton");
    }
}
