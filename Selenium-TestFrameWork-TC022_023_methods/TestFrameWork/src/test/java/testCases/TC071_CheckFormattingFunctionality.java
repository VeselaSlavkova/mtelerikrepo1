package testCases;


import org.junit.*;

public class TC071_CheckFormattingFunctionality extends BaseTest{

    private static final String topicTitle = "TESTING THE FORMATTING";
    private static final String topicMainText = "This test is used for checking the formatting";
    private static final String hyperLinkForTesting = "https://www.youtube.com/watch?v=989TzYHqyH4";
    private static boolean setUpClassBoolean = false;


    @Before
    public void setUpClass() {
        if (!setUpClassBoolean) {
            actions.logIn();
            actions.signIn();
            actions.clickNewTopicButton();
            actions.inputNewTopicTitle(topicTitle);
            setUpClassBoolean = true;
        }

        actions.inputNewTopicMainText(topicMainText);
    }

    @After
    public void clearFieldOFTextArea() {
        actions.clearField("xpath.createTopicTextArea");
    }

    @Test
    public void verifyBoldFormatting() {
        actions.selectAllTextInTextArea();
        actions.applyBoldToTextInTextArea();
        actions.assertElementPresent("//div[@class='d-editor-preview']/p/strong");
    }

    @Test
    public void verifyItalicFormatting() {
        actions.selectAllTextInTextArea();
        actions.applyItalicToTextInTextArea();
        actions.assertElementPresent("//div[@class='d-editor-preview']/p/em");
    }

    @Test
    public void verifyBlockquoteFormatting() {
        actions.selectAllTextInTextArea();
        actions.applyBlockquoteToTextInTextArea();
        actions.assertElementPresent("//div[@class='d-editor-preview']/blockquote");
    }

    @Test
    public void verifyPreformattedTextFormatting() {
        actions.selectAllTextInTextArea();
        actions.applyPreformattedTextToTextInTextArea();
        actions.assertElementPresent("//div[@class='d-editor-preview']/p/code");
    }

    @Test
    public void verifyBulletedListFormatting() {
        actions.selectAllTextInTextArea();
        actions.applyBulletedListToTextInTextArea();
        actions.assertElementPresent("//div[@class='d-editor-preview']/ul");
    }

    @Test
    public void verifyNumberedListFormatting() {
        actions.selectAllTextInTextArea();
        actions.applyNumberedListToTextInTextArea();
        actions.assertElementPresent("//div[@class='d-editor-preview']/ol");
    }


}

