package testCases;

import org.junit.Test;


public class TC019_CreateNewTopic_TextFieldWith_10k_Chars extends BaseTest{


    String topicTitle = "TC019_" + actions.getRandomString(6);
    String topicText = actions.getRandomString(10000);


    @Test
    public void createNewTopic_10000ch_InTextField (){

        actions.logIn();
        actions.signIn();

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);

        //  #### Use script for big text input in few seconds instead of 1-2 minutes with .sendKeys ####

        actions.typeHugeText(topicText, "xpath.createTopicTextArea");

        actions.clickCreateNewTopicButton();
        actions.assertTextEquals(topicTitle, "xpath.createdTopicTitle" );

    }
}
