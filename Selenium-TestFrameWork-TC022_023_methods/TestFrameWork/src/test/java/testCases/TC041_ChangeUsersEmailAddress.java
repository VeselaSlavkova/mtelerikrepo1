package testCases;

import org.junit.Test;

public class TC041_ChangeUsersEmailAddress extends BaseTest {
    String email = "@gmail.com";
    String randomSymbols = actions.getRandomString(10);
    @Test
    public void changeUsersEmailAddress() {
        actions.logIn();
        actions.signIn();

        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.accountCurrentEmailEditIcon");
        actions.clearField("xpath.accountCurrentEmailEditInputField");
        actions.typeValueInField(randomSymbols + email,"xpath.accountCurrentEmailEditInputField");
        actions.clickElement("xpath.accountCurrentEmailChangeButton");

        actions.assertTextEquals("We've sent an email to that address. Please follow the confirmation instructions.","xpath.accountEmailChangedMessage");

    }
}
