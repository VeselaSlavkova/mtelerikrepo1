package testCases;

import org.junit.Test;

public class TC003_CreateTopicAsNotRegisteredUser extends BaseTest {
    @Test
    public void createTopicAsNotRegisteredUser() {
        actions.waitForElementVisible("xpath.logInForumButton", 4);
        actions.assertElementNotPresent("xpath.createTopicButton");
    }
}
