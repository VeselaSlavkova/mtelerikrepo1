package testCases;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class BaseTest {
	public UserActions actions = new UserActions();



	@Before
	public void logIn() {
		actions.logIn();
		actions.signIn();
	}

	@After
	public void logOut() {
		actions.logOut();
	}

	@BeforeClass
	public static void setUp() {
		UserActions.loadBrowser();
	}

	@AfterClass
	public static void tearDown() {
		UserActions.quitDriver();
	}
}
