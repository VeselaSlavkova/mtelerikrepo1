package testCases;

import com.telerikacademy.testframework.CustomWebDriverManager;
import com.telerikacademy.testframework.PropertiesManager;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TC089_UserNotLoginComments extends BaseTest {
    @Test
    public void UserNotLoginComments(){

        //delete all cookie
        Utils.getWebDriver().manage().deleteAllCookies();
        //dynamic wait
        Utils.getWebDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //UserActions.loadBrowser();
        actions.clickElement("xpath.clickOnTheTopic");
        actions.clickElement("xpath.replayButtonIfNotLogin");

        Assert.assertEquals("Telerik Academy","Telerik Academy" );

    }
}

