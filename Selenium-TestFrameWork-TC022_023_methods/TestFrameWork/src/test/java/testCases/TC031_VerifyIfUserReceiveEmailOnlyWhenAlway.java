package testCases;

import org.junit.Test;

public class TC031_VerifyIfUserReceiveEmailOnlyWhenAlway extends BaseTest
{
    @Test
    public void TC0031VerifyReceiveEmailOnlyWhenAlway()
    {
        actions.logIn();
        actions.signIn();

        // Setup email settings to "only when always"
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.emailSettingsButton");
        actions.clickElement("xpath.emailNotificationList");
        actions.comboBoxSelection(1);

        // send private message
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.userMessages");
        actions.clickElement("xpath.userNewMessage");

        actions.typeValueInField("edinev","xpath.userPrivateMessageReceiver");
        actions.clickElement("xpath.userUserNameOfReciever");
        actions.typeValueInField("This is test","xpath.userReplyTitle");
        actions.typeValueInField("Some text here.","xpath.userTextField");
        //actions.clickElement("xpath.userMessageSendButton");
    }
}
