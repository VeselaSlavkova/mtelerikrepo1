package testCases;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import javax.swing.*;
import java.security.Key;
import java.util.concurrent.TimeUnit;

public class TC030_VerifyIfUserReceiveEmailsToAlways extends BaseTest
{
    // Test performed by Emil Dinev
    @Test
    public void TC0030VerifyReceiveEmailToAlways()
    {
        actions.logIn();
        actions.signIn();

        // Setup email settings to "always"
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.emailSettingsButton");
        actions.clickElement("xpath.emailNotificationList");
        actions.comboBoxSelection(0);

        // send private message
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.userMessages");
        actions.clickElement("xpath.userNewMessage");

        actions.typeValueInField("edinev","xpath.userPrivateMessageReceiver");
        actions.clickElement("xpath.userUserNameOfReciever");
        actions.typeValueInField("This is test","xpath.userReplyTitle");
        actions.typeValueInField("Some text here.","xpath.userTextField");
        actions.clickElement("xpath.userMessageSendButton");
    }

}
