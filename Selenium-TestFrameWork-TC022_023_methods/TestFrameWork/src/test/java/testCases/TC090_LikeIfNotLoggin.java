package testCases;

import com.sun.xml.internal.ws.policy.AssertionValidationProcessor;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import javax.swing.*;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;


public class TC090_LikeIfNotLoggin extends BaseTest {
    @Test
    public void LikeIfNotLoggin(){

        //delete all cookie
        Utils.getWebDriver().manage().deleteAllCookies();
        //dynamic wait
        Utils.getWebDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //UserActions.loadBrowser();
        actions.clickElement("xpath.clickOnTheTopic");
       // actions.clickElement("xpath.clickOnTheLikeButton");

        List<WebElement>  buttonLike = new ArrayList<WebElement>();
        buttonLike =  Utils.getWebDriver().findElements(By.xpath(Utils.getUIMappingByKey("xpath.clickOnTheLikeButton")));

        Assert.assertFalse("Assert that element is not visible", buttonLike.get(0).isDisplayed());

    }
}
