package testCases;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSuite extends BaseTest {

    @Test
    public void TC001_CreateATopicAsAUser() {
        String topic = actions.createUniqueTopic();

        actions.clickElement("xpath.createTopicButton"); // Click "New Topic" button
        actions.inputNewTopicTitle(topic);
        actions.inputNewTopicMainText(topic);
        actions.clickElement("xpath.replyAreaCreateTopicButton");
        actions.waitForElementVisible("//*[@id=\"topic-title\"]/div/div/h1/a", 10);
        actions.assertElementPresent("//*[@id=\"topic-title\"]/div/div/h1/a");

    }

    @Test
    public void TC002_EditTopicAsRegisteredUser(){

        //navigate to user dropdown panel
        Utils.LOG.info("Navigating to user dropdown panel");
        actions.waitForElementVisible("xpath.currentUserIcon", 5);
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserIconInUserPanel");

        //select an already created topic
        Utils.LOG.info("Selecting an already created random topic");
        actions.waitForElementVisible("xpath.topicsIconInUserOptions", 5);
        actions.clickElement("xpath.topicsIconInUserOptions");
        actions.clickElement("xpath.randomTopicCreatedByRegisteredUser");

        //edit topic's title
        String editedTitle = "Test " + actions.getRandomString(20);
        Utils.LOG.info("Editing topic title: " + editedTitle);
        actions.waitForElementVisible("xpath.editTopicIcon", 5);
        actions.clickElement("xpath.editTopicIcon");
        actions.clickElement("xpath.editTopicTitleField");
        actions.clearFieldForMacOS("xpath.editTopicTitleField");
        actions.clearField("xpath.editTopicTitleField");
        actions.typeValueInField(editedTitle, "xpath.editTopicTitleField");


        //Select category
        Utils.LOG.info("Selecting a category");
        actions.selectCategory();
        actions.clickElement("//button[@class=\"btn-primary submit-edit btn no-text btn-icon ember-view\"]");

        //Assert topic's title is edited
        String editedTopicTitle = editedTitle;
        String actualEditedTopicTitle = actions.checkEditedTopicTitleExist(editedTitle);
        Assert.assertEquals(editedTopicTitle,actualEditedTopicTitle);

        //Assert category is selected
        actions.assertElementPresent("xpath.siteFeedbackCategory");

    }

    @Test
    public void TC003_CreateTopicAsNotRegisteredUser() {
        actions.logOut();
        actions.waitForElementVisible("xpath.logInForumButton", 4);
        actions.assertElementNotPresent("xpath.createTopicButton");
    }

    @Test
    public void TC004_CreateTopicWithLinkAsTitle() {
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String websiteName = actions.getRandomString(10);
        String firstTitlePart = "www.";
        String lastTitlePart = ".com";

        String topicTitle = firstTitlePart + websiteName + lastTitlePart;
        String topicMainText = "TC04 - Create a new topic with link as a title.";


        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickCreateNewTopicButton();

        actions.checkNewlyCreatedTopicTitleExist(topicTitle);
    }

    @Test
    public void TC005_CreateANewTopicWithCategory() {
        String topicTitle = actions.getRandomString(10);
        String topicMainText = actions.getRandomString(30);

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickElement("xpath.categoryDropDown");
        actions.clickElement("xpath.siteFeedbackCategory");
        actions.clickCreateNewTopicButton();
    }

    @Test
    public void TC006_CreateATopicWithoutTitleAndText() {
        actions.clickNewTopicButton();
        actions.clickCreateNewTopicButton();

        actions.assertElementPresent("xpath.TitleRequiredWarningMsg");
        actions.assertElementPresent("xpath.postCantBeEmptyWarningMsg");
    }

    @Test
    public void TC007_DeletingOwnTopicAsUser() {
        String topicTitle = actions.getRandomString(10);
        String topicMainText = actions.getRandomString(30);

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickCreateNewTopicButton();

        actions.deleteTopic();

    }

    @Test
    public void TC007_DeletingOwnTopicAsUserCHANGED(){

        //navigate to user dropdown panel
        Utils.LOG.info("Navigating to user dropdown panel");
        actions.waitForElementVisible("xpath.currentUserIcon", 5);
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserIconInUserPanel");

        //select an already created topic
        Utils.LOG.info("Selecting an already created random topic");
        actions.waitForElementVisible("xpath.topicsIconInUserOptions", 5);
        actions.clickElement("xpath.topicsIconInUserOptions");
        actions.clickElement("xpath.randomTopicCreatedByRegisteredUser");

        //delete the created topic
        Utils.LOG.info("Delete the selected topic");
        actions.deleteTopic();
    }

    @Test
    public void TC009_CreateNewTopicWith4CharsInTitleField() {

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(getConfigPropertyByKey("topicTitle4charsInput"));
        actions.inputNewTopicMainText(getConfigPropertyByKey("textbox11charsInput"));
        actions.clickNewTopicButtonInPopup();
        actions.assertElementPresent("xpath.topicTitleLengthOutOfBoundaryAlert");
    }

    @Test
    public void TC010_CreateTopicWithFiveTitleChars() {
        String title = actions.getRandomString(5);
        String comment = actions.getRandomString(50);

        actions.clickNewTopicButton();

        //Creating title
        Utils.LOG.info("Creating topic's title");
        actions.clearFieldForMacOS("xpath.createTopicTitleField");
        actions.clearField("xpath.createTopicTitleField");
        actions.inputNewTopicTitle(title);

        //Creating comment
        Utils.LOG.info("Filling in topic's first comment");
        actions.inputNewTopicMainText(comment);
        actions.clickCreateNewTopicButton();

        //Assert topic is created
        actions.returnToHomePage();
        String createdTopicTitle = title;
        String actualTopicTitle = actions.checkNewlyCreatedTopicTitleExist(title);
        Assert.assertEquals(createdTopicTitle,actualTopicTitle);
        Utils.LOG.info("Topic is created");

    }

    @Test
    public void TC11_CreateTopicWithFiveTitleChars() {
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String title = actions.getRandomString(6);
        String comment = actions.getRandomString(50);

        Utils.LOG.info("Logging into TASF website");

        actions.clickNewTopicButton();

        //Creating title
        Utils.LOG.info("Creating topic's title");
        actions.clearFieldForMacOS("xpath.createTopicTitleField");
        actions.clearField("xpath.createTopicTitleField");
        actions.inputNewTopicTitle(title);

        //Creating comment
        Utils.LOG.info("Filling in topic's first comment");
        actions.inputNewTopicMainText(comment);
        actions.clickCreateNewTopicButton();

        //Assert topic is created
        actions.returnToHomePage();
        String createdTopicTitle = title;
        String actualTopicTitle = actions.checkNewlyCreatedTopicTitleExist(title);
        Assert.assertEquals(createdTopicTitle,actualTopicTitle);
        Utils.LOG.info("Topic is created");
    }

    @Test
    public void TC012_NewTopicWithHundredChars(){

        String title = actions.createUniqueTopic()+"Lorem Ipsum is simply dummy text of the printing and typesetting e";
        String text = "text popular Ipsum is not simply random belief, Lorem ";


        actions.clickNewTopicButton();
        actions.clearField("xpath.createTopicTitleField");
        actions.inputNewTopicTitle(title);
        actions.inputNewTopicMainText(text);
        actions.clickCreateNewTopicButton();
        actions.returnToHomePage();
        String createdTopicTitle = title;
        String actualTopicTitle = actions.checkNewlyCreatedTopicTitleExist(title);
        Assert.assertEquals(createdTopicTitle,actualTopicTitle);
    }

    @Test
    public void TC014_CreateTopicWith255Chars()
    {

        actions.clickNewTopicButton();
        actions.clearField("xpath.createTopicTitleField");

        actions.inputNewTopicTitle("TC0014 Create new topic with 255 chars.");
        String text255Chars = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget" +
                "dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus" +
                "mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,";

        actions.clearField("xpath.createTopicTextArea");
        actions.inputNewTopicMainText(text255Chars);
    }

    @Test
    public void TC019_CreateNewTopicTextFieldWith10kChars (){

        String topicTitle = "TC019_" + actions.getRandomString(6);
        String topicText = actions.getRandomString(10000);

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);

        //  #### Use script for big text input in few seconds instead of 1-2 minutes with .sendKeys ####

        actions.typeHugeText(topicText, "xpath.createTopicTextArea");

        actions.clickCreateNewTopicButton();
        actions.assertTextEquals(topicTitle, "xpath.createdTopicTitle" );
    }

    @Test
    public void TC020_CreateNewTopicTextFieldWith32kChars (){
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String topicTitle = "TC20_" + actions.getRandomString(6);
        String topicText = actions.getRandomString(32000);

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);

        //  #### Use script for the big text input in few seconds instead of 1-2 minutes with .sendKeys ####

        actions.typeHugeText(topicText,"xpath.createTopicTextArea");

        actions.clickCreateNewTopicButton();
        actions.assertTextEquals(topicTitle, "xpath.createdTopicTitle" );
    }



    @Test
    public void TC022_ConfirmYesAbandonButtonWorks() throws InterruptedException {

        String title = actions.getRandomString(25);
        String description = actions.getRandomString(50);
        //create a topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(title);
        actions.inputNewTopicMainText(description);
        //cancel frame for creating a topic
        actions.cancelTopicOrComment();
        //verify "Yes,Abandon" button is active
        actions.abandonTopicFrame();
        actions.assertElementNotPresent("xpath.newTopicAbandonConfirmationMsg");
    }

    @Test
    public void TC023_ConfirmNoKeenButtonWorks(){

        String title = actions.getRandomString(25);
        String description = actions.getRandomString(50);
        //create a topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(title);
        actions.inputNewTopicMainText(description);
        //cancel frame for creating a topic
        actions.cancelTopicOrComment();
        //verify "No,Keen" button is active
        actions.keepOnTopicFrame();
        actions.assertElementNotPresent("xpath.newTopicAbandonConfirmationMsg");
    }

    @Test
    public void TC024_ConfirmEscXButtonWorks() {
        actions.clickElement("xpath.createTopicButton");
        actions.inputNewTopicTitle("TestTestTest");
        actions.inputNewTopicMainText("TestTestTest");
        actions.clickElement("xpath.createTopicEscXButton");
        actions.assertElementNotPresent("xpath.createTopicEscXButton");
    }

    @Test
    public void TC025_ChangeTopicName() {
        String topic = actions.createUniqueTopic();
        String topicEdited = topic + "-Edited";

        //Create a Topic

        //create new topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topic);
        actions.inputNewTopicMainText(topic);
        actions.clickElement("xpath.replyAreaCreateTopicButton");

        //Edit a Topic
        actions.waitForElementVisible("xpath.editTopicPencilIcon", 10);
        actions.clickElement("xpath.editTopicPencilIcon");

        actions.waitForElementVisible("xpath.editTitleField", 10);
        actions.clearField("xpath.editTitleField");
        actions.typeValueInField(topicEdited, "xpath.editTitleField");
        actions.clickElement("xpath.confirmEditTopicThickIcon");
        actions.assertTextEquals(topicEdited, "//a[@class='fancy-title']");
    }

    @Test
    public void TC026_AddVoteToATopic(){

        final String VOTES_STATUS_LIMITED = "Limit";
        final String VOTES_STATUS_VOTED = "Voted";

        String topic = actions.createUniqueTopic();

        //Create a Topic

        //create new topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topic);
        actions.inputNewTopicMainText(topic);
        actions.clickElement("xpath.replyAreaCreateTopicButton");
        actions.waitForElementVisible("//div[@class='vote-count']", 10);

        //get votes before voting

        String currentVotesStatus = actions.getText("xpath.votesStatus");

        if (!currentVotesStatus.equals(VOTES_STATUS_LIMITED) &&
                !currentVotesStatus.equals(VOTES_STATUS_VOTED)){
            int votesBeforeVoting = Integer.parseInt(actions.getText("xpath.votesForATopic"));
            actions.clickElement("xpath.voteButton");
            int votesAfterVoting = Integer.parseInt(actions.getText("xpath.votesForATopic"));
            int differenceInVotes = votesAfterVoting- votesBeforeVoting;
            Assert.assertTrue(differenceInVotes == 1);
        }
    }

    @Test
    public void TC028_CreateNewTopicWithOptionalTag(){

        actions.clickNewTopicButton();

        // Enter topic title.
        String titleTopic = actions.createUniqueTopic();
        actions.inputNewTopicTitle(titleTopic);

        //input text in topic's body
        String bodyText = actions.getRandomString(35);
        actions.inputNewTopicMainText(bodyText);

        //push button create topic and postTopic
        actions.clickCreateNewTopicFinalButton();
    }

    @Test
    public void TC029_VerifySearchFnInOptionalTags() {

        String tagInput = "materials";

        actions.clickNewTopicButton();
        actions.clickElement("xpath.tagSelector");
        actions.typeValueInField(tagInput, "xpath.tagSelectorInputField");

        actions.assertTextEquals(tagInput, "xpath.resultTag", "title");
        actions.cancelTopicOrComment();
    }

    @Test
    public void TC030_VerifyReceiveEmailToAlways()
    {

        // Setup email settings to "always"
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.emailSettingsButton");
        actions.clickElement("xpath.emailNotificationList");
        actions.comboBoxSelection(0);

        // send private message
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.userMessages");
        actions.clickElement("xpath.userNewMessage");

        actions.typeValueInField("edinev","xpath.userPrivateMessageReceiver");
        actions.clickElement("xpath.userUserNameOfReciever");
        actions.typeValueInField("This is test","xpath.userReplyTitle");
        actions.typeValueInField("Some text here.","xpath.userTextField");
        actions.clickElement("xpath.userMessageSendButton");
    }

    @Test
    public void TC031_VerifyReceiveEmailOnlyWhenAlway()
    {

        // Setup email settings to "only when always"
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.emailSettingsButton");
        actions.clickElement("xpath.emailNotificationList");
        actions.comboBoxSelection(1);

        // send private message
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.userMessages");
        actions.clickElement("xpath.userNewMessage");

        actions.typeValueInField("edinev","xpath.userPrivateMessageReceiver");
        actions.clickElement("xpath.userUserNameOfReciever");
        actions.typeValueInField("This is test","xpath.userReplyTitle");
        actions.typeValueInField("Some text here.","xpath.userTextField");
        //actions.clickElement("xpath.userMessageSendButton");
    }

    @Test
    public void TC039_ChangeName() {

        String newName = "Test 01 Telerik ";
        String randomSymbols = actions.getRandomString(10);

        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.waitForElementVisible("xpath.accountName", 10);
        actions.clearField("xpath.accountName");
        actions.typeValueInField(newName + randomSymbols, "xpath.accountName");
        actions.clickElement("xpath.profileSaveChangesButton");

        actions.assertTextEquals("Saved!", "xpath.profileSavedMessage" );
        actions.assertTextEquals( newName + randomSymbols, "xpath.fullName");
    }

    @Test
    public void TC041_ChangeUsersEmailAddress() {

        String email = "@gmail.com";
        String randomSymbols = actions.getRandomString(10);

        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.accountCurrentEmailEditIcon");
        actions.clearField("xpath.accountCurrentEmailEditInputField");
        actions.typeValueInField(randomSymbols + email,"xpath.accountCurrentEmailEditInputField");
        actions.clickElement("xpath.accountCurrentEmailChangeButton");

        actions.assertTextEquals("We've sent an email to that address. Please follow the confirmation " +
                "instructions.","xpath.accountEmailChangedMessage");
    }

    @Test
    public void TC042_ChangeProfileFeatures() {

        String location = "City ";
        String randomSymbols = actions.getRandomString(10);

        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.userProfileNavigationButton");
        actions.clearField("xpath.profileLocationInputField");
        actions.typeValueInField(location + randomSymbols,"xpath.profileLocationInputField");
        actions.clickElement("xpath.profileSaveChangesButton");

        actions.assertTextEquals("Saved!", "xpath.profileSavedMessage" );
        actions.assertElementAttributeValueEquals(location + randomSymbols,
                "xpath.profileLocationInputField",
                "value");
    }

    @Test
    public void TC059_CreateACommentForExistingTopic(){
        String comment = "Replying to a post";

        //Find topic in forum
        actions.waitForElementVisible("xpath.searchFirstTopicFromMainList", 10);
        actions.clickElement("xpath.searchFirstTopicFromMainList");

        //Create a comment
        actions.waitForElementVisible("xpath.replyOnCommentButton", 10);
        actions.clickElement("xpath.replyOnCommentButton");
        actions.waitForElementVisible("xpath.createTopicTextArea", 10);
        actions.typeValueInField(comment, "xpath.createTopicTextArea");

        //Assert comment is created
        actions.clickElement("xpath.replyAreaCreateTopicButton");
        actions.assertElementNotPresent("xpath.cancelReplyOnCommentButton");
    }

    @Test
    public void TC063_CreateValidCommentAndCancel(){

        String someText = "This is a text for the textarea to be used";

        actions.waitForElementVisible("xpath.searchFirstTopicFromMainList",10);
        actions.clickElement("xpath.searchFirstTopicFromMainList");
        actions.waitForElementVisible("xpath.replyOnCommentButton",10);
        actions.clickElement("xpath.replyOnCommentButton");
        actions.waitForElementVisible("xpath.createTopicTextArea",10);
        actions.clickElement("xpath.createTopicTextArea");
        actions.typeValueInField(someText,"xpath.createTopicTextArea");
        actions.clickElement("xpath.cancelReplyOnCommentButton");
        actions.waitForElementVisible("xpath.newTopicAbandonButton",10);
        actions.clickElement("xpath.newTopicAbandonButton");
        actions.assertElementNotPresent("xpath.newTopicAbandonButton");
    }

    @Test
    public void TC089_UserNotLoginComments(){

        actions.logOut();
        actions.clickElement("xpath.clickOnTheTopic");
        actions.clickElement("xpath.replayButtonIfNotLogin");

        Assert.assertEquals("Telerik Academy","Telerik Academy" );
    }

    @Test
    public void TC090_LikeIfNotLogin(){

        actions.clickElement("xpath.clickOnTheTopic");
        // actions.clickElement("xpath.clickOnTheLikeButton");

        List<WebElement> buttonLike = new ArrayList<WebElement>();
        buttonLike =  Utils.getWebDriver().findElements(By.xpath(Utils.getUIMappingByKey("xpath.clickOnTheLikeButton")));

        Assert.assertFalse("Assert that element is not visible", buttonLike.get(0).isDisplayed());
    }

    @Test
    public void TC091_ShareIfNotLogIn(){

        actions.clickElement("xpath.clickOnTheTopic");
        actions.clickElement("xpath.ClickShareButton");

        Assert.assertEquals("https://schoolforum.telerikacademy.com/t/o3dlbs4aq5/554",
                "https://schoolforum.telerikacademy.com/t/o3dlbs4aq5/554");
    }

    @Test
    public void TC092_QuoteOption() {

        actions.clickElement("xpath.clickOnTheTopic");
        actions.clickElement("xpath.replayButtonIfUserIsLogIn");

        Assert.assertEquals("Hello", "Hello");
    }
}