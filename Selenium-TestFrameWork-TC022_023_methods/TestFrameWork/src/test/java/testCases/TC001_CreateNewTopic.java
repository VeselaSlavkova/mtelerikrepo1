package testCases;

import org.junit.Test;

public class TC001_CreateNewTopic extends BaseTest {

    @Test
    public void createNewTopicWithMinimumStringLength(){

        String title = actions.getRandomString(5);
        String description = actions.getRandomString(10);

        actions.logIn();
        actions.signIn();
        actions.clickCreateNewTopicButton();
        actions.inputNewTopicTitle(title);
        actions.inputNewTopicMainText(description);
        actions.clickCreateNewTopicFinalButton();

        actions.assertElementNotPresent("xpath.createNewTopicFinalButton");
    }

}
