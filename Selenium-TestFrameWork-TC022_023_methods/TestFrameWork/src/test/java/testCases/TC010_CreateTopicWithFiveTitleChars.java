package testCases;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TC010_CreateTopicWithFiveTitleChars extends BaseTest{

    String title = actions.getRandomString(5);
    String comment = actions.getRandomString(50);

    @Before
    public void logIn(){
        Utils.LOG.info("Logging into TASF website");
        actions.logIn();
        actions.signIn();
    }

    @Test
    public void createTopicWithFiveTitleChars() {

        actions.clickNewTopicButton();

        //Creating title
        Utils.LOG.info("Creating topic's title");
        actions.clearFieldForMacOS("xpath.createTopicTitleField");
        actions.clearField("xpath.createTopicTitleField");
        actions.inputNewTopicTitle(title);

        //Creating comment
        Utils.LOG.info("Filling in topic's first comment");
        actions.inputNewTopicMainText(comment);
        actions.clickCreateNewTopicButton();

        //Assert topic is created
        actions.returnToHomePage();
        String createdTopicTitle = title;
        String actualTopicTitle = actions.checkNewlyCreatedTopicTitleExist(title);
        Assert.assertEquals(createdTopicTitle,actualTopicTitle);
        Utils.LOG.info("Topic is created");


    }
}
