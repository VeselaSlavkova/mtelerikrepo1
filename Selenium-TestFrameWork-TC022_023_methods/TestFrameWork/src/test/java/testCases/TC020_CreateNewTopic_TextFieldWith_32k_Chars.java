package testCases;

import org.junit.Test;

public class TC020_CreateNewTopic_TextFieldWith_32k_Chars extends BaseTest{

    String topicTitle = "TC20_" + actions.getRandomString(6);
    String topicText = actions.getRandomString(32000);


    @Test
    public void createNewTopic_32000ch_InTextField (){

        actions.logIn();
        actions.signIn();

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);

        //  #### Use script for the big text input in few seconds instead of 1-2 minutes with .sendKeys ####

        actions.typeHugeText(topicText,"xpath.createTopicTextArea");

        actions.clickCreateNewTopicButton();
        actions.assertTextEquals(topicTitle, "xpath.createdTopicTitle" );

    }
}