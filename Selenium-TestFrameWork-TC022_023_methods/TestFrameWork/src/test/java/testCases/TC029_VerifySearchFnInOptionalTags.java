package testCases;

import org.junit.Test;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class TC029_VerifySearchFnInOptionalTags extends BaseTest {

    @Test
    public void verifySearchFnInOptionalTags() {

        String tagInput = "materials";

        actions.logIn();
        actions.signIn();
        actions.clickNewTopicButton();
        actions.clickElement("xpath.tagSelector");
        actions.typeValueInField(tagInput, "xpath.tagSelectorInputField");

        actions.assertTextEquals(tagInput, "xpath.resultTag", "title");
        actions.cancelTopicOrComment();
    }
}
