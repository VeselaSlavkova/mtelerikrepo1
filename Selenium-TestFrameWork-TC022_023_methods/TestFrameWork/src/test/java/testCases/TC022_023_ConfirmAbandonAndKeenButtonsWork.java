package testCases;

import org.junit.Test;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class TC022_023_ConfirmAbandonAndKeenButtonsWork extends BaseTest {
    String title = actions.getRandomString(25);
    String description = actions.getRandomString(50);

    @Test
    public void ConfirmYesAbandonButtonWorks() throws InterruptedException {
        actions.logIn();
        actions.signIn();
        //create a topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(title);
        actions.inputNewTopicMainText(description);
        //cancel frame for creating a topic
        actions.cancelTopicOrComment();
        //verify "Yes,Abandon" button is active
        actions.abandonTopicFrame();
        actions.assertElementNotPresent("xpath.newTopicAbandonConfirmationMsg");
    }
    @Test
    public void ConfirmNoKeepButtonWorks(){
        //create a topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(title);
        actions.inputNewTopicMainText(description);
        //cancel frame for creating a topic
        actions.cancelTopicOrComment();
        //verify "No,Keep" button is active
        actions.keepOnTopicFrame();
        actions.assertElementNotPresent("xpath.newTopicAbandonConfirmationMsg");
    }
}
