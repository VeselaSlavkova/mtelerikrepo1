package testCases;

import org.junit.Test;

public class TC025_ChangeTopicName extends BaseTest {

    @Test
    public void ChangeTopicName() {
        String topic = actions.createUniqueTopic();
        String topicEdited = topic + "-Edited";

        //Create a Topic
        actions.logIn();
        actions.signIn();

        //create new topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topic);
        actions.inputNewTopicMainText(topic);
        actions.clickElement("xpath.replyAreaCreateTopicButton");

        //Edit a Topic
        actions.waitForElementVisible("xpath.editTopicPencilIcon", 10);
        actions.clickElement("xpath.editTopicPencilIcon");

        actions.waitForElementVisible("xpath.editTitleField", 10);
        actions.clearField("xpath.editTitleField");
        actions.typeValueInField(topicEdited, "xpath.editTitleField");
        actions.clickElement("xpath.confirmEditTopicThickIcon");
        actions.assertTextEquals(topicEdited, "//a[@class='fancy-title']");
    }
}
