package testCases;

import org.junit.Test;

public class TC039_ChangeName extends BaseTest {
    String newName = "Test 01 Telerik ";
    String randomSymbols = actions.getRandomString(10);

    @Test
    public void changeName() {
        actions.logIn();
        actions.signIn();

        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.waitForElementVisible("xpath.accountName", 10);
        actions.clearField("xpath.accountName");
        actions.typeValueInField(newName + randomSymbols, "xpath.accountName");
        actions.clickElement("xpath.profileSaveChangesButton");

        actions.assertTextEquals("Saved!", "xpath.profileSavedMessage" );
        actions.assertTextEquals( newName + randomSymbols, "xpath.fullName");

    }
}