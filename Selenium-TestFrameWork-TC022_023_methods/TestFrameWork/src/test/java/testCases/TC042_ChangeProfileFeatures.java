package testCases;

import org.junit.Test;

public class TC042_ChangeProfileFeatures extends BaseTest{
    String location = "City ";
    String randomSymbols = actions.getRandomString(10);
    @Test
    public void changeUsersEmailAddress() {
        actions.logIn();
        actions.signIn();

        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserOptionsPanelPreferencesIcon");
        actions.clickElement("xpath.userProfileNavigationButton");
        actions.clearField("xpath.profileLocationInputField");
        actions.typeValueInField(location + randomSymbols,"xpath.profileLocationInputField");
        actions.clickElement("xpath.profileSaveChangesButton");

        actions.assertTextEquals("Saved!", "xpath.profileSavedMessage" );
        actions.assertElementAttributeValueEquals(location + randomSymbols,
                                                  "xpath.profileLocationInputField",
                                                  "value");
    }
}
