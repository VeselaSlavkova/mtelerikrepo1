package testCases;

import org.junit.Test;

public class TC014_CreateTopicWith255Chars extends BaseTest
{
    // Test performed by Emil Dinev
    @Test
    public void TC0014_createTopicWith255Chars()
    {
        actions.logIn();
        actions.signIn();
        actions.clickNewTopicButton();
        actions.clearField("xpath.createTopicTitleField");

        actions.inputNewTopicTitle("TC0014 Create new topic with 255 chars.");
        String text255Chars = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget" +
                "dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus" +
                "mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,";

        actions.clearField("xpath.createTopicTextArea");
        actions.inputNewTopicMainText(text255Chars);
    }
}
