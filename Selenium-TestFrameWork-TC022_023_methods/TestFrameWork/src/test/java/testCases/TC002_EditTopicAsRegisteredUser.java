package testCases;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TC002_EditTopicAsRegisteredUser extends BaseTest {

    @Before
    public void logIn(){
        Utils.LOG.info("Logging into TASF website");
        actions.logIn();
        actions.signIn();
    }

    @Test
    public void editTopicAsRegisteredUser(){

        //navigate to user dropdown panel
        Utils.LOG.info("Navigating to user dropdown panel");
        actions.waitForElementVisible("xpath.currentUserIcon", 5);
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserIconInUserPanel");

        //select an already created topic
        Utils.LOG.info("Selecting an already created random topic");
        actions.waitForElementVisible("xpath.topicsIconInUserOptions", 5);
        actions.clickElement("xpath.topicsIconInUserOptions");
        actions.clickElement("xpath.randomTopicCreatedByRegisteredUser");

        //edit topic's title
        String editedTitle = "Test " + actions.getRandomString(20);
		Utils.LOG.info("Editing topic title: " + editedTitle);
		actions.waitForElementVisible("xpath.editTopicIcon", 5);
        actions.clickElement("xpath.editTopicIcon");
        actions.clickElement("xpath.editTopicTitleField");
        actions.clearFieldForMacOS("xpath.editTopicTitleField");
        actions.clearField("xpath.editTopicTitleField");
        actions.typeValueInField(editedTitle, "xpath.editTopicTitleField");

        //Select category
        Utils.LOG.info("Selecting a category");
        actions.selectCategory();

        //Create label
//        Utils.LOG.info("Creating label");
//        actions.createLabel();


        //Check if topic's category allows creating a specific label
//        boolean specificCategoryNotAllowingRandomLabelsError = actions.specificCategoryNotAllowingRandomLabelsError();
//        System.out.println(specificCategoryNotAllowingRandomLabelsError);
//        if (actions.specificCategoryNotAllowingRandomLabelsError()){
//        if (specificCategoryNotAllowingRandomLabelsError){
//            Utils.LOG.info("Category not allowing randomly created label error appear");
//            actions.clickElement("xpath.editTagsDropdown");
//            actions.clickElement("xpath.deleteCreatedLabelButton");
//        } else {
//        actions.waitForElementVisible("xpath.specificCategoryNotAllowingRandomLabelsError", 5);
//        actions.assertElementPresent("xpath.specificCategoryNotAllowingRandomLabelsError");
//        actions.clickElement("xpath.specificCategoryNotAllowingRandomLabelsOKButton");
//        actions.clickElement("xpath.editTagsDropdown");
//        actions.clickElement("xpath.deleteCreatedLabelButton");
            //Submit changes
            Utils.LOG.info("Submitting changes");
            actions.clickElement("xpath.confirmEditTopicThickIcon");
//        }

//        actions.driverWait();

        //Assert topic's title is edited
        String editedTopicTitle = editedTitle;
        String actualEditedTopicTitle = actions.checkEditedTopicTitleExist(editedTitle);
        Assert.assertEquals(editedTopicTitle,actualEditedTopicTitle);

        //Assert category is selected
        actions.assertElementPresent("xpath.siteFeedbackCategory");

    }
}
