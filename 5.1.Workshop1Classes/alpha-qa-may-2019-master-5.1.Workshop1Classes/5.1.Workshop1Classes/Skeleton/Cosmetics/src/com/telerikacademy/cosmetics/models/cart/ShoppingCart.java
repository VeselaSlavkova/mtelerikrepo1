package com.telerikacademy.cosmetics.models.cart;

import com.telerikacademy.cosmetics.models.products.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> productList;

    public ShoppingCart() {
        productList = new ArrayList<Product> ();

    }

    public List<Product> getProductList() {

        return productList;
    }
    public void addProduct(Product product) {
        if(product!=null) {
            productList.add (product);
        }else{
            throw new IllegalArgumentException("Invalid product.");
        }
    }

    public void removeProduct(Product product) {
        if(product!=null) {
        productList.remove (product);
    }else{
        throw new IllegalArgumentException("Invalid command.");
    }
    }
    public boolean containsProduct(Product product) {
        if (productList.size () == 0) {
            throw new IllegalArgumentException();
        }
        return productList.contains(product);
    }

        public double totalPrice() {
            double total=0.0;
        for (Product product:productList) {
           total+=product.getPrice ();
        }
        return total;
    }
}
