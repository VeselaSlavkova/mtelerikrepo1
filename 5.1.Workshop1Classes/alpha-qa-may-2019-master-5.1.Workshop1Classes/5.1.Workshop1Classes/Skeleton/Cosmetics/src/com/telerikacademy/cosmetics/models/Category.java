package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.products.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Category {
    private final static int NAME_LENGTH_MIN = 2;
    private final static int NAME_LENGTH_MAX = 15;
    private String name;
    private ArrayList<Product> products;

    public Category(String name) {
        setName (name);
        products = new ArrayList<> ();
    }

    public List<Product> getProducts() {
        return products;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length () < NAME_LENGTH_MIN || name.length () > NAME_LENGTH_MAX) {
            throw new IllegalArgumentException ("Invalid name of the category.");
        }
        this.name = name;
    }

    public void addProduct(Product product) {
        products.add (product);
    }

    public void removeProduct(Product product) {
        if (products.size () > 0) {
            products.remove (product);
        } else {
            throw new NotImplementedException ();
        }

    }
private String s="";
    public String print() {
        if (products.size () == 0) {
            s= String.format ("#Category: %s\n" +
                    " #No product in this category", name);
        }else{
        for (Product product : products) {
           s= String.format ("#Category: %s\n#%s %s\nPrice: %f\nGender: %s\n===", name, product.getName (), product.getBrand (), product.getPrice (), product.getGender ());
        }
        }
        return s;
    }
}
  // Format:
            //"#Category [Category Name]
            // #[Name] [Brand]
            // #Price: [Price]
            // #Gender: [Gender]
            // ==="

