package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


public class Product {
    private final static int NAME_LENGTH_MIN = 3;
    private final static int NAME_LENGTH_MAX = 10;
    private final static int BRAND_LENGTH_MAX = 10;
    private final static int BRAND_LENGTH_MIN = 2;
    private final static int PRICE_MIN = 0;

    private String name;
    private String brand;
    private double price;
    private GenderType gender;


    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < NAME_LENGTH_MIN || name.length() > NAME_LENGTH_MAX) {
            throw new IllegalArgumentException("Invalid name of the product.");
        }

        this.name = name;
    }
    public String getBrand() {

        return brand;
    }
    public void setBrand(String brand) {
        if ( brand.length () < BRAND_LENGTH_MIN || brand.length () > BRAND_LENGTH_MAX ) {
            throw new IllegalArgumentException ("Invalid name of the brand.");
        } else {
            this.brand = brand;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if ( price > PRICE_MIN ) {
            this.price = price;
        } else {
            throw new IllegalArgumentException ("Invalid price.");
        }

    }

    public GenderType getGender() {

        return gender;
    }

    public void setGender(GenderType gender) {
        if ( gender == GenderType.MEN || gender == GenderType.WOMAN || gender == GenderType.UNISEX ) {
            this.gender = gender;
        } else {
            throw new IllegalArgumentException ("Invalid type of customer.");
        }
    }

    public String print() {
        String productPrint =String.format (" #%s %s\n #Price: %f\n #Gender: %s\n===",name,brand,price,gender) ;
        return productPrint;
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="

    }
}
