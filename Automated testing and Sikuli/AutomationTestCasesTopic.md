 ###  Created Automation Tests cover the Test Cases for the following forum funcionalities:

|**Test Case ID:** |**Test Title**|**Priority**|
|------|-------|--------|
|**TC-1**|Creation of a new topic with valid input|**1**|
|**TC-2**|Creation of a new topic with empty title and empty text section|**1**|
|**TC-3**|Creation of a new topic with under sized title and under sized text section|**1**|
|**TC-4**|Creation of a new topic with a link for title and with attached image in text section |**2**|
|**TC-5**|Cancel creation of a new topic|**1**|

-------
|**Pre requisite:**|OS: Windows, 10 Browsers: Chrome; FireFox,Valid user account in the forum https://schoolforum.telerikacademy.com/||
|------|-------|--------|
|**Test designed by:**|Vesela Slavkova||
|**Test design date**|08.09.2019||

----
---------
|**Test Case ID:**|**Test Case Description:**|**Steps to reproduse**|**Input data**|**Expected Output**|
|-------------|----|------------|-------------|----------------|
| **TC-1**|Creation of a new topic with valid input| Log in | |The User logged in successfully|
|TC-1.sikuli.zip ||Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
|||Enter topic title in ‘Title field’|*Test topic*|‘Title field’is active for typing in it|
|||In the field for category select one from drop-down menu|*Site feedback*|Selected category is displayed  in the relevant field|
| || Enter text in text field|*Find all as many bugs as you can*|‘Textbox field’is active for typing in it|
||| Click "+ Create Topic" button| | The new Topic is visible for all users|
|**Test Case ID:**|**Test Case Description:**|**Steps to reproduse**|**Input data**|**Expected Output**|
| **TC-2**|Creation of a new topic with empty field for title and empty text section| Log in | |The User logged in successfully|
|TC-2.sikuli.zip ||Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
||| Type nothing in the title field||Pop up **Message** *Title is required*|
||| Type nothing in the textbox field||Pop up **Message** *Post can``t be empty*|
||| Click "+ Create Topic" button| | The new Topic is not created|
|**Test Case ID:**|**Test Case Description:**|**Steps to reproduse**|**Input data**|**Expected Output**|
| **TC-3**|Creation of a new topic with under sized title /4 characters/ and under sized text section /4 characters/| Log in | |The User logged in successfully|
|TC-3.sikuli.zip ||Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
||| Enter topic title in ‘Title field’|*Test*|Pop up **Message** *Title must be at least 5 characters*|
||| Enter text in text field|*text*|Pop up **Message** *Post must be at least 10 characters*|
||| Click "+ Create Topic" button| | The new Topic is not created|
|**Test Case ID:**|**Test Case Description:**|**Steps to reproduse**|**Input data**|**Expected Output**|
| **TC-4**|Creation of a new topic with a link for title and with attached image in text section | Log in | |The User logged in successfully|
|TC-4.sikuli.zip ||Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
||| Enter link for title in ‘Title field’|"https://www.evilenglish.net/say-vs-tell/"|Link is visible and active|
||| Attach image in text field|"https://www.evilenglish.net/wp-content/uploads/2016/05/p03bm6tf.jpg"|Link for image is visible and active|
||| Click "+ Create Topic" button| | The new Topic is created|
| **TC-5**|Cancel creation of a new topic,made by mistake| Log in | |The User logged in successfully|
|TC-5.sikuli.zip ||Click  "+ New Topic" button|  |A frame to create a new topic gets displayed|
||| Enter topic title in ‘Title field’|"Test topic123"|‘Title field’is active for typing in it|
||| Enter text in text field|*testtext1213*|‘Textbox’is active for typing in it|
||| Click "Cancel" button| | TA frame to create a topic canceled.New Topic is not created|